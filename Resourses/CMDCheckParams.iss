﻿// © Kotyarko_O, 2016 \\

[Code]
Const
 NoSearchGameFiles = '/NOSEARCHGAMEFILES';
 NoCheckForMutex = '/NOCHECKFORMUTEX';
 NoCheckForRun = '/NOCHECKFORRUN';
 Updated = '/UPDATED';
 Silent = '/SILENT';

Function CMDCheckParams(Param: String): Boolean;
var
 I: Integer;
begin
 for I := 1 to ParamCount do
 begin
  Result := Uppercase(ParamStr(I)) = Param;
  if Result then
   Exit;
 end;
end;