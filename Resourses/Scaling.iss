﻿// © Kotyarko_O, 2016 \\

[Code]
Function GetDeviceCaps(hDC, nIndex: Integer): Integer; external 'GetDeviceCaps@GDI32 stdcall';
Function GetDC(HWND: DWord): DWord; external 'GetDC@user32.dll stdcall';

Const
 Scale100 = 96;
 Scale125 = 120;
 Scale150 = 144;

Var
 DisplayScaled: Boolean;

Function Scaling(Value: Integer): Integer;
begin
 case GetDeviceCaps(GetDC(0), 88) of
  Scale100: Result := Value;
  Scale125: Result := Round(Value + (Value / 4.5));
  Scale150: Result := Round(Value + (Value / 2.0{?}));
 end;
end;

Procedure DisplayScale();
begin
 case GetDeviceCaps(GetDC(0), 88) of
  Scale100: DisplayScaled := False;
  Scale125:
  begin
   MsgBoxEx(WizardForm.Handle, 'Внимание!' + #13#10 + 'Степень масштабирования изображения: 125%.' + #13#10 +
   'Некоторые элементы интерфейса установщика могут отображаться некорректно.',
   'Масштабирование', MB_OK or MB_ICONWARNING, 0, 0);
   DisplayScaled := True;
  end;
  Scale150:
  begin
   MsgBoxEx(WizardForm.Handle, 'Внимание!' + #13#10 + 'Степень масштабирования изображения: 150%.' + #13#10 +
   'Рекомендуется отключить или снизить степень масштабирования элементов в системе для корректного отображения интерфейса установщика.',
   'Масштабирование', MB_OK or MB_ICONWARNING, 0, 0);
   DisplayScaled := True;
   Abort;
  end;
 end;
end;