﻿// © Kotyarko_O, 2016 \\

[Files]
Source: "FILES\*.png"; Flags: dontcopy
Source: "FILES\Images_Background\ListsBackground.bmp"; Flags: dontcopy

[Icons]
Name: "{app}\KMP\{#UninstallShortcut}"; Filename: "{uninstallexe}"; WorkingDir: "{app}\KMP"; Comment: "Деинсталляция сборки."; IconFilename: "{uninstallexe}";

[Code]
Const
 GWL_EXSTYLE = -20;
 WS_EX_COMPOSITED = $02000000;

Type
 TModImagePos = record
  Top, Left, Width, Height: Integer;
 end;

Var
 XVMPage, PMODPage, TweakerPage: TWizardPage;
 KRLogo, XVMLogo, WOTLogo, AcesLogo: Longint;
 ErrCode: Integer;
 FolderTreeView: TFolderTreeView;
 DiskSpaceLabel, BackupHeaderLabel,
 ReadyLabel, InstStatusLabel, InstallProgressLabel, InstFilenameLabel, InstLabel,
 KRLogoClickLbl, XVMLogoClickLbl, WOTLogoClickLbl, AcesLogoClickLbl: TLabel;
 RBDelete, RBBackup, RBNone, CBCleanProfile, CBParamsRemember, CBReport, CBCreateUninstallIcon, CBGameLaunch: HWND;
 XVMList, PMODList, TweakerList: TNewCheckListBox;
 ModImagePos: TModImagePos;
 DescriptionMemo: TMemo;

Function GetWindowLongForListsBG(hWnd: HWND; nIndex: Integer): Longint; external 'GetWindowLongW@user32.dll stdcall';
Function SetWindowLongForListsBG(hWnd: HWND; nIndex: Integer; dwNewLong: Longint): Longint; external 'SetWindowLongW@user32.dll stdcall';

Procedure FolderTreeViewOnChange(Sender: TObject);
begin
 WizardForm.DirEdit.Text := AddBackslash(FolderTreeView.Directory);
end;

Procedure ChangeConfiguratorStatus(FileName: String);
begin
 InstStatusLabel.Caption := 'Настройка конфигураций...';
 InstFilenameLabel.Caption := FileName;
end;

Procedure ChangeInstallStatus(Status: String);
begin
 InstStatusLabel.Caption := Status;
 InstFilenameLabel.Caption := ExpandConstant(CurrentFilename);
 InstallProgressLabel.Caption := 'Выполнено: ' + Format('%.1d', [(WizardForm.ProgressGauge.Position * 100) / WizardForm.ProgressGauge.Max]) + '%';
end;

Procedure LaunchGame(CurStep: TSetupStep);
begin
 if CurStep = ssDone then
  if CheckBoxGetChecked(CBGameLaunch) then
   Exec(ExpandConstant('{app}\WOTLauncher.exe'), '', ExpandConstant('{app}'), SW_SHOW, ewNoWait, ErrCode);
end;

Procedure UninstallCheckBoxClick(hBtn: HWND);
begin
 if CheckBoxGetChecked(hBtn) then
 begin
  if not FileExists(ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk')) then
   FileCopy(ExpandConstant('{app}\KMP\{#UninstallShortcut}.lnk'), ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk'), False);
 end else
  if FileExists(ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk')) then
   DeleteFile(ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk'));
end;

Procedure LabelsOnClick(Sender: TObject);
begin
 case TLabel(Sender) of
  KRLogoClickLbl: ShellExec('', '{#KRURL}', '', '', SW_SHOW, ewNoWait, ErrCode);
  XVMLogoClickLbl: ShellExec('', '{#XVMSite}', '', '', SW_SHOW, ewNoWait, ErrCode);
  WOTLogoClickLbl: ShellExec('', '{#WOTSite}', '', '', SW_SHOW, ewNoWait, ErrCode);
  AcesLogoClickLbl: ShellExec('', '{#ACESSite}', '', '', SW_SHOW, ewNoWait, ErrCode);
 end;
end;

Function AddCheckBoxExt(CheckListBox: TNewCheckListBox; Caption, SubCaption: String; Level: Integer; Enabled: Boolean; FontStyle: TFontStyles): Integer;
begin
 Result := CheckListBox.AddCheckBox(Caption, SubCaption, Level, False, Enabled, True, True, nil);
 CheckListBox.ItemFontStyle[Result] := FontStyle;
end;

Function AddRadioButtonExt(CheckListBox: TNewCheckListBox; Caption, SubCaption: String; Level: Integer; Enabled: Boolean; FontStyle: TFontStyles): Integer;
begin
 Result := CheckListBox.AddRadioButton(Caption, SubCaption, Level, False, Enabled, nil);
 CheckListBox.ItemFontStyle[Result] := FontStyle;
end;

Procedure CreateObjects();
var
 I, ObjLeft, ObjTop, ObjWidth, ObjHeight: Integer;
begin
 ExtractTemporaryFile('ListsBackground.bmp');

 KRLogo := ImgLoad(WizardForm.Handle, 'KRLogo.png', Scaling(8), WizardForm.ClientHeight - 44, 0, 0, False, False);
 XVMLogo := ImgLoad(WizardForm.Handle, 'XVMLogo.png', Scaling(60), WizardForm.ClientHeight - 41, 0, 0, False, False);
 WOTLogo := ImgLoad(WizardForm.Handle, 'WOTLogo.png', Scaling(104), WizardForm.ClientHeight - 44, 0, 0, False, False);
 AcesLogo := ImgLoad(WizardForm.Handle, 'ACESLogo.png', Scaling(154), WizardForm.ClientHeight - 44, 0, 0, False, False);

 KRLogoClickLbl := TLabel.Create(WizardForm);
 with KRLogoClickLbl do
 begin
  Parent := WizardForm;
  ImgGetPosition(KRLogo, ObjLeft, ObjTop, ObjWidth, ObjHeight);
  SetBounds(ObjLeft, ObjTop, ObjWidth, ObjHeight);
  Cursor := crHand;
  Transparent := True;
  OnClick := @LabelsOnClick;
 end;
 XVMLogoClickLbl := TLabel.Create(WizardForm);
 with XVMLogoClickLbl do
 begin
  Parent := WizardForm;
  ImgGetPosition(XVMLogo, ObjLeft, ObjTop, ObjWidth, ObjHeight);
  SetBounds(ObjLeft, ObjTop, ObjWidth, ObjHeight);
  Cursor := crHand;
  Transparent := True;
  OnClick := @LabelsOnClick;
 end;
 WOTLogoClickLbl := TLabel.Create(WizardForm);
 with WOTLogoClickLbl do
 begin
  Parent := WizardForm;
  ImgGetPosition(WOTLogo, ObjLeft, ObjTop, ObjWidth, ObjHeight);
  SetBounds(ObjLeft, ObjTop, ObjWidth, ObjHeight);
  Cursor := crHand;
  Transparent := True;
  OnClick := @LabelsOnClick;
 end;
 AcesLogoClickLbl := TLabel.Create(WizardForm);
 with AcesLogoClickLbl do
 begin
  Parent := WizardForm;
  ImgGetPosition(AcesLogo, ObjLeft, ObjTop, ObjWidth, ObjHeight);
  SetBounds(ObjLeft, ObjTop, ObjWidth, ObjHeight);
  Cursor := crHand;
  Transparent := True;
  OnClick := @LabelsOnClick;
 end;

 with WizardForm.InfoBeforeMemo do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(10), Scaling(100), WizardForm.ClientWidth - Scaling(20), Scaling(380));
  ScrollBars := ssVertical;
  ReadOnly := True;
  Font.Size := 10;
 end;

 DiskSpaceLabel := TLabel.Create(WizardForm);
 with DiskSpaceLabel do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(155), Scaling(100), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 10;
  Caption := WizardForm.DiskSpaceLabel.Caption;
 end;

 CBReport := CheckBoxCreate(WizardForm.Handle, DiskSpaceLabel.Left, DiskSpaceLabel.Top + DiskSpaceLabel.Height + Scaling(3), Scaling(390), Scaling(20), 'CheckBox.png', 0, 2);
  CheckBoxSetText(CBReport, 'Отправить отчёт о конфигурации этой установки (анонимно).');
  CheckBoxSetFont(CBReport, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(CBReport, clWhite, $CCCCCC, $D9D9D9, clGray);
   #ifdef SendReport
  CheckBoxSetChecked(CBReport, not FileExists(ExpandConstant('{src}\Log.KMP')));
   #else
  CheckBoxSetEnabled(CBReport, False);
  #endif

 with WizardForm.DirEdit do
 begin
  Parent := WizardForm;
  SetBounds(DiskSpaceLabel.Left, DiskSpaceLabel.Top + DiskSpaceLabel.Height + Scaling(26), WizardForm.ClientWidth - DiskSpaceLabel.Left - Scaling(15), WizardForm.DirEdit.Height + Scaling(5));
  Font.Size := 9;
 end;
 FolderTreeView := TFolderTreeView.Create(WizardForm);
 with FolderTreeView do
 begin
  Parent := WizardForm;
  SetBounds(WizardForm.DirEdit.Left + Scaling(5), WizardForm.DirEdit.Top + WizardForm.DirEdit.Height + 10, WizardForm.DirEdit.Width - Scaling(6), Scaling(130));
  Directory := RemoveBackslash(WizardForm.DirEdit.Text);
  OnChange := @FolderTreeViewOnChange;
 end;
 
 BackupHeaderLabel := TLabel.Create(WizardForm);
 with BackupHeaderLabel do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(20), FolderTreeView.Top + FolderTreeView.Height + Scaling(32), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := WizardForm.Font.Size;
  Caption := 'Для избежания проблем совместимости и конфликтов версий, рекомендуется выбрать вариант очистки папки с модами.';
  Alignment := taCenter;
 end;

 RBDelete := CheckBoxCreate(WizardForm.Handle, Scaling(20), BackupHeaderLabel.Top + Scaling(BackupHeaderLabel.Height + 5), Scaling(425), Scaling(20), 'RadioButton.png', 1, 2);
  CheckBoxSetText(RBDelete, 'Полная очистка игрового клиента от установленных модификаций.');
  CheckBoxSetFont(RBDelete, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(RBDelete, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetChecked(RBDelete, True);
 RBBackup := CheckBoxCreate(WizardForm.Handle, Scaling(20), BackupHeaderLabel.Top + Scaling(BackupHeaderLabel.Height + 30), Scaling(475), Scaling(20), 'RadioButton.png', 1, 2);
  CheckBoxSetText(RBBackup, 'Создание резервной копии установленных в данный момент модификаций.');
  CheckBoxSetFont(RBBackup, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(RBBackup, clWhite, $CCCCCC, $D9D9D9, clGray);
 RBNone := CheckBoxCreate(WizardForm.Handle, Scaling(20), BackupHeaderLabel.Top + Scaling(BackupHeaderLabel.Height + 55), Scaling(535), Scaling(20), 'RadioButton.png', 1, 2);
  CheckBoxSetText(RBNone, 'Не предпринимать никаких действий по отношению к установленным модификациям.');
  CheckBoxSetFont(RBNone, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(RBNone, clWhite, $CCCCCC, $D9D9D9, clGray);

 CBCleanProfile := CheckBoxCreate(WizardForm.Handle, Scaling(20), BackupHeaderLabel.Top + Scaling(BackupHeaderLabel.Height + 80 + 5), Scaling(495), Scaling(22), 'CheckBox.png', 0, 2);
  CheckBoxSetText(CBCleanProfile, 'Очистка профиля игрового клиента (сброс индивидуальных настроек и кэша).');
  CheckBoxSetFont(CBCleanProfile, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(CBCleanProfile, clWhite, $CCCCCC, $D9D9D9, clGray);
 CBParamsRemember := CheckBoxCreate(WizardForm.Handle, Scaling(20), BackupHeaderLabel.Top + Scaling(BackupHeaderLabel.Height + 105 + 5), Scaling(455), Scaling(22), 'CheckBox.png', 0, 2);
  CheckBoxSetText(CBParamsRemember, 'Запомнить выбранные параметры текущей установки (рекомендуется).');
  CheckBoxSetFont(CBParamsRemember,  WizardForm.Font.Handle);
  CheckBoxSetFontColor(CBParamsRemember, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetChecked(CBParamsRemember, not FileExists(ExpandConstant('{src}\Log.KMP')));

 with WizardForm.ComponentsList do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(10), Scaling(93), Scaling(455), Scaling(389));
  Offset := 2;
  Font.Size := 9;
  if not DisplayScaled then
  begin
   LoadBGBmpFromFile(ExpandConstant('{tmp}\ListsBackground.bmp'), 0, 0);
   SetWindowLongForListsBG(Handle, GWL_EXSTYLE, GetWindowLongForListsBG(Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED);
  end;
  for I := 0 to ItemCount - 1 do
   if (ItemLevel[I] = 0) or (ItemLevel[I] = 1) then
    ItemFontStyle[I] := [fsBold];
 end;

 with ModImagePos do
 begin
  Left := WizardForm.ComponentsList.Left + WizardForm.ComponentsList.Width + Scaling(10);
  Top := WizardForm.ComponentsList.Top - Scaling(1);
  Width := 275;
  Height := Scaling(295);
 end;

 DescriptionMemo := TMemo.Create(WizardForm);
 with DescriptionMemo do
 begin
  Parent := WizardForm;
  SetBounds(ModImagePos.Left - Scaling(1), WizardForm.ComponentsList.Top + ModImagePos.Height + Scaling(2), ModImagePos.Width + Scaling(2), Scaling(92));
  ReadOnly := True;
  HideSelection := True;
  Text := 'Наведите курсор на компонент в списке, чтобы увидеть его описание и скриншот.';
  Font.Color := clWhite;
  Font.Size := 9;
  DragMode := dmAutomatic;
 end;

 XVMPage := CreateCustomPage(wpSelectComponents, '', '');
 XVMList := TNewCheckListBox.Create(WizardForm);
 with XVMList do
 begin
  Parent := WizardForm;
  SetBounds(WizardForm.ComponentsList.Left, WizardForm.ComponentsList.Top, WizardForm.ComponentsList.Width, WizardForm.ComponentsList.Height);
  Offset := 2;
  Tag := 2;
  Font.Size := WizardForm.ComponentsList.Font.Size;
  if not DisplayScaled then
  begin
   LoadBGBmpFromFile(ExpandConstant('{tmp}\ListsBackground.bmp'), 0, 0);
   SetWindowLongForListsBG(Handle, GWL_EXSTYLE, GetWindowLongForListsBG(Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED);
  end;

  AddCheckBoxExt(XVMList, 'Extended visualization mod (XVM):', '13,1 Мб', 0, True, [fsBold]);

  AddCheckBoxExt(XVMList, 'Окно логина:', '', 1, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'автоматический вход в игру.', '0,1 Мб', 2, True, []);
  AddCheckBoxExt(XVMList, 'сохранять последний выбранный сервер.', '0,1 Мб', 2, True, []);

  AddCheckBoxExt(XVMList, 'Ангарный интерфейс:', '', 1, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'отображать пинг серверов.', '0,1 Мб', 2, True, []);
  AddCheckBoxExt(XVMList, 'часы в ангаре.', '0,1 Мб', 2, True, []);
  AddCheckBoxExt(XVMList, 'автовозврат съёмного оборудования.', '0,1 Мб', 2, True, []);
  AddCheckBoxExt(XVMList, 'Карусель танков в ангаре:', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'значки мастерства.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'процент побед.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'количество боёв.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'показатель количества отличительных отметок.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'показатель среднего урона за бой.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'xTE - эффективность по танку.', '0,1 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Боевой интерфейс:', '', 1, True, [fsBold]);

  AddCheckBoxExt(XVMList, 'Режим цветовой слепоты:', '', 2, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'стандартный вариант.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'альтернативный вариант.', '0,1 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Иконки танков:', '', 2, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'от Betax, вариант 1.', '1,9 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'от Betax, вариант 2.', '1,9 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'в стиле xobotyi, от galagan.', '1,8 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'от Black_Spy.', '3,3 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'от Witblitz.', '1,8 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'от seriych.', '1,8 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Окно загрузки боя и список игроков по Tab:', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'выделять игроков с активными сетевыми сервисами.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'отображать флаги языковых настроек игроков.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'отображать количество боёв на технике.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'отображать % побед игроков.', '0,1 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Опции миникарты:', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'отображать ХП на миникарте.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'индикация взводных игроков на миникарте.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'индикация обнаруженных союзников на миникарте.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'рентген-круг.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'круг максимального радиуса засвета.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'динамический круг обзора.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'круг максимальной отрисовки.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'выделять ТТ-10 отдельными маркерами.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'Увеличение (центрирование) миникарты:', '', 3, True, []);
  AddRadioButtonExt(XVMList, 'по нажатию левого Ctrl.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'по нажатию левого Alt.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'по нажатию CapsLock.', '0,1 Мб', 4, True, []);
  AddCheckBoxExt(XVMList, 'Альтернативный режим миникарты:', '', 3, True, []);
  AddCheckBoxExt(XVMList, 'отображать никнеймы игроков.', '0,1 Мб', 4, True, []);

  AddCheckBoxExt(XVMList, 'Панели игроков ("уши" команд):', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'отображать ХП (по Alt).', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'Отображение статуса засвета:', '', 3, True, []);
  AddRadioButtonExt(XVMList, 'вариант 1 (стандартный).', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'вариант 2 (точки).', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'вариант 3 (звёздочки).', '0,1 Мб', 4, True, []);
  AddCheckBoxExt(XVMList, 'выделять уничтоженных врагов.', '0,1 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Маркеры игроков:', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'индикатор низкого запаса прочности.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'значки классности.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'индикация обнаруженных союзников в маркерах.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'флаги языковых настроек в маркерах.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'активность сетевых сервисов в маркерах.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'Индикация взводных игроков в маркерах:', '', 3, True, []);
  AddCheckBoxExt(XVMList, 'в стандартном режиме.', '0,1 Мб', 4, True, []);
  AddCheckBoxExt(XVMList, 'в альтернативном режиме (по Alt).', '0,1 Мб', 4, True, []);

  AddCheckBoxExt(XVMList, 'Дополнительные текстовые поля:', '', 2, True, [fsBold]);
  AddCheckBoxExt(XVMList, 'отображение силы команд.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'таймер перезарядки.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'заблокированный урон.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'урон, нанесённый с вашей помощью.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'таймер ремонта модулей.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, 'лог полученного урона.', '', 3, True, []);
  AddCheckBoxExt(XVMList, 'Лог нанесённого урона (хит-лог):', '', 3, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'простой.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'детальный.', '0,1 Мб', 4, True, []);
  AddCheckBoxExt(XVMList, 'Информационные панели:', '', 3, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'вариант от nigth_dragon_on.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'вариант от Kotyarko_O.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'полный вариант.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'облегчённый вариант.', '0,1 Мб', 4, True, []);
  AddCheckBoxExt(XVMList, 'Расчёт эффективности в бою:', '', 3, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'по формуле WN8.', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'по рейтингу эффективности (РЭ).', '0,1 Мб', 4, True, []);
  AddRadioButtonExt(XVMList, 'объединённый (WN8 + РЭ).', '0,1 Мб', 4, True, []);

  AddCheckBoxExt(XVMList, 'Модифицированные лампочки 6-го чувства:', '', 2, True, [fsBold]);
  AddRadioButtonExt(XVMList, 'стандартная XVM.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'lol.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'педобир.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'красный стоп-сигнал.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'лампа в кругу.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'простая лампа.', '0,1 Мб', 3, True, []);
  AddRadioButtonExt(XVMList, 'око.', '0,1 Мб', 3, True, []);
  AddCheckBoxExt(XVMList, '9-ти секундная лампочка шестого чувства.', '0,1 Мб', 3, True, []);

  AddCheckBoxExt(XVMList, 'Озвучка лампочки 6-го чувства (экспериментально):', '', 2, False, [fsBold]);
  AddRadioButtonExt(XVMList, 'стандартный сигнал.', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'голосовое уведомление.', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'уведомление как на Rudy.', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'сигнал с отсчётом (10 секунд).', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'сигнал без отсчёта.', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'око Саурона.', '0,1 Мб', 3, False, []);

  AddCheckBoxExt(XVMList, 'Озвучка критического попадания (экспериментально):', '', 2, False, [fsBold]);
  AddRadioButtonExt(XVMList, 'звонок крита.', '0,1 Мб', 3, False, []);
  AddRadioButtonExt(XVMList, 'звонок крита с голосовым уведомлением.', '0,1 Мб', 3, False, []);

  AddCheckBoxExt(XVMList, 'Дополнительная озвучка (экспериментально):', '', 2, False, [fsBold]);
  AddCheckBoxExt(XVMList, 'звук обнаружения противника.', '0,1 Мб', 3, False, []);
  AddCheckBoxExt(XVMList, 'звук пожара.', '0,1 Мб', 3, False, []);
  AddCheckBoxExt(XVMList, 'звук повреждения боеукладки.', '0,1 Мб', 3, False, []);
 end;

 PMODPage := CreateCustomPage(XVMPage.ID, '', '');
 PMODList := TNewCheckListBox.Create(WizardForm);
 with PMODList do
 begin
  Parent := WizardForm;
  SetBounds(WizardForm.ComponentsList.Left, WizardForm.ComponentsList.Top, WizardForm.ComponentsList.Width, WizardForm.ComponentsList.Height);
  Offset := 2;
  Tag := 3;
  Font.Size := WizardForm.ComponentsList.Font.Size;
  if not DisplayScaled then
  begin
   LoadBGBmpFromFile(ExpandConstant('{tmp}\ListsBackground.bmp'), 0, 0);
   SetWindowLongForListsBG(Handle, GWL_EXSTYLE, GetWindowLongForListsBG(Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED);
  end;

  AddCheckBoxExt(PMODList, 'Комплексная модификация PMOD:', '2,41 Мб', 0, True, [fsBold]);
  AddCheckBoxExt(PMODList, 'Убрать затемнение в снайперском режиме.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Отключить эффекты динамичесой камеры.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Включить горизонтальную стабилизацию.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Свободная камера (в реплеях).', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Максимальное отдаление (командирская камера).', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Отключить красное моргание при попадании по вам.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Отключение ручного тормоза на ПТ-САУ.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Индикатор бронепробиваемости.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(PMODList, 'Фильтр сообщений в ангаре.', '0,1 Мб', 1, True, []);

  AddCheckBoxExt(PMODList, 'Дополнительный маркер серверного сведения в прицеле:', '', 1, True, [fsBold]);
  AddRadioButtonExt(PMODList, 'стандартный вариант маркера.', '0,3 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'модернизированный вариант маркера.', '0,3 Мб', 2, True, []);

  AddCheckBoxExt(PMODList, 'Увеличение зума снайперского прицела:', '', 1, True, [fsBold]);
  AddRadioButtonExt(PMODList, 'до х32.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'до х50.', '0,1 Мб', 2, True, []);

  AddCheckBoxExt(PMODList, 'Стартовый зум снайперского прицела:', '', 1, True, [fsBold]);
  AddRadioButtonExt(PMODList, '2-х.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, '6-х.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, '24-х.', '0,1 Мб', 2, True, []);

  AddCheckBoxExt(PMODList, 'ХП команд:', '', 1, True, [fsBold]);
  AddRadioButtonExt(PMODList, 'оформление от Armagomen.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'оформление от Locastan.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'оформление Minimal.', '0,1 Мб', 2, True, []);

  AddCheckBoxExt(PMODList, 'Сессионная статистика:', '', 1, True, [fsBold]);
  AddRadioButtonExt(PMODList, 'стандартный вариант.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от Kotyarko_O.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от vlad_cs_sr.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от Meddio.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от Xxx_mutant.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от demon2597.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от Antoshkaaa.', '0,1 Мб', 2, True, []);
  AddRadioButtonExt(PMODList, 'вариант от Armagomen.', '0,1 Мб', 2, True, []);
 end;

 TweakerPage := CreateCustomPage(PMODPage.ID, '', '');
 TweakerList := TNewCheckListBox.Create(WizardForm);
 with TweakerList do
 begin
  Parent := WizardForm;
  SetBounds(WizardForm.ComponentsList.Left, WizardForm.ComponentsList.Top, WizardForm.ComponentsList.Width, WizardForm.ComponentsList.Height);
  Offset := 2;
  Tag := 4;
  Font.Size := WizardForm.ComponentsList.Font.Size;
  if not DisplayScaled then
  begin
   LoadBGBmpFromFile(ExpandConstant('{tmp}\ListsBackground.bmp'), 0, 0);
   SetWindowLongForListsBG(Handle, GWL_EXSTYLE, GetWindowLongForListsBG(Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED);
  end;

  AddCheckBoxExt(TweakerList, 'Wot Tweaker:', '', 0, True, [fsBold]);
  AddCheckBoxExt(TweakerList, 'Отключить загрузку эмблем.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Отключить многоядерность.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать туман (дымку) на картах.', '0,6 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать дым от уничтоженной техники.', '0,2 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать дым и пламя при выстреле.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать дым из выхлопной трубы техники.', '0,3 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать облака.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать движение деревьев.', '0,1 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать эффект уничтожения техники.', '0,2 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать эффекты взрыва снарядов и попадания в объекты.', '0,6 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать эффект попадания в танк.', '0,2 Мб', 1, True, []);
  AddCheckBoxExt(TweakerList, 'Убрать эффект проявления погоды и дым от объектов.', '0,4 Мб', 1, True, []);
 end;

 ReadyLabel := TLabel.Create(WizardForm);
 with ReadyLabel do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(15), Scaling(94), 0, 0);
  AutoSize := True;
  Transparent := True;
  WordWrap := False;
  Font.Size := 11;
  Font.Style := [fsBold];
  Caption := 'Нажмите «Установить», чтобы продолжить, или «Назад», чтобы изменить опции установки.';
 end;
 with WizardForm.ReadyMemo do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(10), ReadyLabel.Top + Scaling(ReadyLabel.Height), WizardForm.ClientWidth - Scaling(20), Scaling(375));
  Font.Size := 9;
 end;

 InstStatusLabel := TLabel.Create(WizardForm);
 with InstStatusLabel do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(15), Scaling(93), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 11;
  Font.Style := [fsBold];
  Caption := WizardForm.StatusLabel.Caption;
 end;
 InstallProgressLabel := TLabel.Create(WizardForm);
 with InstallProgressLabel do
 begin
  Parent := WizardForm;
  SetBounds(WizardForm.ClientWidth - Scaling(150), InstStatusLabel.Top, 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 11;
  Font.Style := [fsBold];
 end;
 with WizardForm.ProgressGauge do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(15), InstStatusLabel.Top + Scaling(InstStatusLabel.Height + 10), WizardForm.ClientWidth - Scaling(30), Scaling(30));
 end;
 InstFilenameLabel := TLabel.Create(WizardForm);
 with InstFilenameLabel do
 begin
  Parent := WizardForm;
  SetBounds(InstStatusLabel.Left, WizardForm.ProgressGauge.Top + WizardForm.ProgressGauge.Height + Scaling(10), WizardForm.ClientWidth - Scaling(35), Scaling(15));
  AutoSize := False;
  Transparent := True;
  Font.Size := 9;
  Font.Style := [fsBold];
  Caption := WizardForm.FilenameLabel.Caption;
 end;
 InstLabel := TLabel.Create(WizardForm);
 with InstLabel do
 begin
  Parent := WizardForm;
  SetBounds(InstFilenameLabel.Left, InstFilenameLabel.Top + Scaling(InstFilenameLabel.Height + 16), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 10;
  Font.Style := [fsBold];
  Caption := 'Пожалуйста, дождитесь завершения установки {#MyAppName}...';
 end;

 CBCreateUninstallIcon := CheckBoxCreate(WizardForm.Handle, Scaling(20), Scaling(405), Scaling(445), Scaling(22), 'CheckBox.png', 0, 2);
  CheckBoxSetText(CBCreateUninstallIcon, 'Создать ярлык на рабочем столе для быстрой деинсталляции сборки?');
  CheckBoxSetFont(CBCreateUninstallIcon, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(CBCreateUninstallIcon, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetEvent(CBCreateUninstallIcon, BtnClickEventID, WrapBtnCallback(@UninstallCheckBoxClick, 1));
 CBGameLaunch := CheckBoxCreate(WizardForm.Handle, Scaling(20), Scaling(430), Scaling(300), Scaling(22), 'CheckBox.png', 0, 2);
  CheckBoxSetText(CBGameLaunch, 'Запустить игру после закрытия установщика?');
  CheckBoxSetFont(CBGameLaunch, WizardForm.PageDescriptionLabel.Font.Handle);
  CheckBoxSetFontColor(CBGameLaunch, clWhite, $CCCCCC, $D9D9D9, clGray);
end;

Function _IsComponentSelected(CheckListBox: TNewCheckListBox; Name: String): Boolean;
var
 I, Idx: Integer;
begin
 Result := False;
 try
  if Name = 'CheckForChecked' then
  begin
   for I := 0 to CheckListBox.ItemCount - 1 do
   begin
    Result := CheckListBox.Checked[I];
    if Result then
     Exit;
   end;
  end else
  begin
   Idx := CheckListBox.Items.IndexOf(Name);
   for I := 0 to CheckListBox.ItemCount - 1 do
    if (Idx <= CheckListBox.ItemCount) and (Idx = I) then
    begin
     Result := CheckListBox.Checked[Idx];
     if Result then
      Exit;
    end;
  end;
 except
  MsgBoxEx(WizardForm.Handle, GetExceptionMessage(), '{#__FILE__}: {#__LINE__}', MB_ICONERROR or MB_OK, 0, 0);
 end;
end;

Function ComponentsChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(WizardForm.ComponentsList, Name);
end;

Function XVMChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(XVMList, Name);
end;

Function PMODChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(PMODList, Name);
end;

Function TweakerChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(TweakerList, Name);
end;