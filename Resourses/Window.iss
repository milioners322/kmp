﻿// © Kotyarko_O, 2016 \\

[Code]
Procedure Window();
var
 WO, HO: Integer;
begin
 WO := 250;
 HO := 140;
 with WizardForm do
 begin
  ClientWidth := WizardForm.Width + Scaling(WO);
  ClientHeight := WizardForm.Height + Scaling(HO);
  OuterNotebook.Hide;
  InnerNotebook.Hide;
  Bevel.Hide;
  BackButton.SetBounds(BackButton.Left + Scaling(WO - 2), WizardForm.ClientHeight - 38, BackButton.Width + 5, BackButton.Height + 3);
  NextButton.SetBounds(NextButton.Left + Scaling(WO + 5), WizardForm.ClientHeight - 38, NextButton.Width + 5, NextButton.Height + 3);
  CancelButton.SetBounds(CancelButton.Left + Scaling(WO + 5), WizardForm.ClientHeight - 38, CancelButton.Width + 5, CancelButton.Height + 3);
 end;
 WizardForm.Center;
 case GetDeviceCaps(GetDC(0), 88) of
  Scale100: WizardForm.Font.Size := 10;
  Scale125, Scale150: WizardForm.Font.Size := 9;
 end;
end;
