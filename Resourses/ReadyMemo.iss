﻿// © Kotyarko_O, 2016 \\

[Code]
Function LineIndent(Space: String; Level: Integer): String;
var
 I: Integer;
begin
 SetLength(Space, Length(Space) / 2);
 for I := 0 to Level do
  Result := Result + Space;
end;

Function CustomListToReadyMemo(CheckListBox: TNewCheckListBox; Space, NewLine, NewLineSpace: String): String;
var
 I: Integer;
begin
 if _IsComponentSelected(CheckListBox, 'CheckForChecked') then
 begin
  Result := Result + NewLine;
  with CheckListBox do
   for I := 0 to Items.Count - 1 do
    if Checked[I] then
     Result := Result + NewLineSpace + LineIndent(Space, ItemLevel[I]) + ItemCaption[I];
 end;
end;

Function _UpdateReadyMemo(Space, NewLine, MemoDirInfo: String): String;
var
 LineSpace: String;
begin
 LineSpace := NewLine + Space;

 if CheckBoxGetChecked(RBDelete) or CheckBoxGetChecked(RBBackup) or CheckBoxGetChecked(RBNone) or CheckBoxGetChecked(CBCleanProfile) then
  Result := Result + 'Функции работы с клиентом:';
 if CheckBoxGetChecked(RBDelete) then
  Result := Result + LineSpace + CheckBoxGetText(RBDelete);
 if CheckBoxGetChecked(RBBackup) then
  Result := Result + LineSpace + CheckBoxGetText(RBBackup);
 if CheckBoxGetChecked(RBNone) then
  Result := Result + LineSpace + CheckBoxGetText(RBNone);
 if CheckBoxGetChecked(CBCleanProfile) then
  Result := Result + LineSpace + CheckBoxGetText(CBCleanProfile);
  
 if CheckBoxGetChecked(CBParamsRemember) or CheckBoxGetChecked(CBReport) then
  Result := Result + NewLine + NewLine + 'Дополнительные опции установки:';
 if CheckBoxGetChecked(CBParamsRemember) then
  Result := Result + LineSpace + CheckBoxGetText(CBParamsRemember);
 if CheckBoxGetChecked(CBReport) then
  Result := Result + LineSpace + CheckBoxGetText(CBReport);

 Result := Result + NewLine + NewLine;
 Result := Result + MemoDirInfo + NewLine + NewLine;
 Result := Result + 'Выбранные компоненты:';

 Result := Result + CustomListToReadyMemo(WizardForm.ComponentsList, Space, '', NewLine + Space);
 Result := Result + CustomListToReadyMemo(XVMList, Space, NewLine, NewLine + Space);
 Result := Result + CustomListToReadyMemo(PMODList, Space, NewLine, NewLine + Space);
 Result := Result + CustomListToReadyMemo(TweakerList, Space, NewLine, NewLine + Space);
end;