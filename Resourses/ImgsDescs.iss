﻿// © Kotyarko_O, 2016 \\

#include AddBackslash(SourcePath) + "ImagesAndDescriptions.iss"

[Files]
Source: "FILES\Images_Components\*"; Excludes: "ImgSize-*,*.psd"; Flags: recursesubdirs nocompression dontcopy

[Code]
Var
 ModImage: Longint;

Procedure _OnItemMouseMove(Sender: TObject; X, Y, Index: Integer; Area: TItemArea);
begin
 if Index > -1 then
 begin
  Descriptions(Sender, Index, DescriptionMemo);
  Images(Sender, Index, ModImage);
  if not TNewCheckListBox(Sender).ItemEnabled[Index] then
   DescriptionMemo.Text := DescriptionMemo.Text + ' Временно недоступно.';
  ImgApplyChanges(WizardForm.Handle);
 end;
end;

Procedure _OnMouseLeave(Sender: TObject);
begin
 ImgRelease(ModImage);
 ModImage := ImgLoad(WizardForm.Handle, 'KMP.png', ModImagePos.Left, ModImagePos.Top, 0, 0, False, False);
 DescriptionMemo.Text := 'Наведите курсор мыши на компонент в списке, чтобы увидеть его описание и скриншот.';
 ImgApplyChanges(WizardForm.Handle);
end;

Procedure InitializeImgsDescs();
begin
 ModImage := ImgLoad(WizardForm.Handle, 'KMP.png', ModImagePos.Left, ModImagePos.Top, 0, 0, False, False);
 WizardForm.ComponentsList.OnItemMouseMove := @_OnItemMouseMove;
 WizardForm.ComponentsList.OnMouseLeave := @_OnMouseLeave;
 XVMList.OnItemMouseMove := @_OnItemMouseMove;
 XVMList.OnMouseLeave := @_OnMouseLeave;
 PMODList.OnItemMouseMove := @_OnItemMouseMove;
 PMODList.OnMouseLeave := @_OnMouseLeave;
 TweakerList.OnItemMouseMove := @_OnItemMouseMove;
 TweakerList.OnMouseLeave := @_OnMouseLeave;
end;