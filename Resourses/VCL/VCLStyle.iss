﻿[Files]
Source: "Resourses\VCL\VCLStylesInno.dll"; Flags: dontcopy
Source: "Resourses\VCL\{#VCL}.vsf"; Flags: dontcopy

[Code]
Procedure LoadVCLStyle(VClStyleFile: String); external 'LoadVCLStyleW@files:VCLStylesInno.dll stdcall delayload';
Procedure UnLoadVCLStyles(); external 'UnLoadVCLStyles@files:VCLStylesInno.dll stdcall delayload';

Procedure VCLInitializeSetup();
begin
 ExtractTemporaryFile('{#VCL}.vsf');
 LoadVCLStyle(ExpandConstant('{tmp}\{#VCL}.vsf'));
end;
