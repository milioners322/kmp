﻿// © Kotyarko_O, 2016 \\

[Files]
Source: "FILES\Images_Background\*"; Excludes: "ImgSize-*,*.bmp"; Flags: dontcopy

[Code]
Var
  #ifdef UpdateVersion
 UpdPagePage, UPDPageIDP,
 #endif
 Background, wpWelcomePage, wpInfoBeforePage, wpSelectDirPage, wpSelectComponentsPage,
 XVMPagePage, PMODPagePage, TweakerPagePage, wpReadyPage, wpInstallingPage, wpFinishedPage: Longint;

Procedure CreatePagesImages();
begin
 Background := ImgLoad(WizardForm.Handle, 'Background.jpg', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpWelcomePage := ImgLoad(WizardForm.Handle, 'wpWelcome.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
  #ifdef UpdateVersion
 UPDPageIDP := ImgLoad(WizardForm.Handle, 'UPDPage_IDP.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 UpdPagePage := ImgLoad(WizardForm.Handle, 'UPDPage.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 #endif
 wpInfoBeforePage := ImgLoad(WizardForm.Handle, 'wpInfoBefore.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpSelectDirPage := ImgLoad(WizardForm.Handle, 'wpSelectDir.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpSelectComponentsPage := ImgLoad(WizardForm.Handle, 'wpSelectComponents.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 XVMPagePage := ImgLoad(WizardForm.Handle, 'XVMPage.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 PMODPagePage := ImgLoad(WizardForm.Handle, 'PMODPage.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 TweakerPagePage := ImgLoad(WizardForm.Handle, 'TweakerPage.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpReadyPage := ImgLoad(WizardForm.Handle, 'wpReady.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpInstallingPage := ImgLoad(WizardForm.Handle, 'wpInstalling.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
 wpFinishedPage := ImgLoad(WizardForm.Handle, 'wpFinished.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, DisplayScaled, True);
end;

Procedure HideObjects();
begin
 ImgSetVisibility(wpWelcomePage, False);
  #ifdef UpdateVersion
 ImgSetVisibility(UpdPagePage, False);
 ImgSetVisibility(UPDPageIDP, False);
 #endif
 ImgSetVisibility(wpInfoBeforePage, False);
 ImgSetVisibility(wpSelectDirPage, False);
 ImgSetVisibility(wpSelectComponentsPage, False);
 ImgSetVisibility(XVMPagePage, False);
 ImgSetVisibility(PMODPagePage, False);
 ImgSetVisibility(TweakerPagePage, False);
 ImgSetVisibility(wpReadyPage, False);
 ImgSetVisibility(wpInstallingPage, False);
 ImgSetVisibility(wpFinishedPage, False);
 ImgSetVisibility(ModImage, False);
 //
 WizardForm.InfoBeforeMemo.Hide;
 //
  #ifdef UpdateVersion
 UpdCheckButton.Hide;
 UpdMemo.Hide;
 UpdText.Hide;
 AvailableUpd.Hide;
 InfoAboutUpd.Hide;
 DownloadStartButton.Hide;
 UpdProgressGauge.Hide;
 #endif
 //
 DiskSpaceLabel.Hide;
 CheckBoxSetVisibility(CBReport, False);
 WizardForm.DirEdit.Hide;
 FolderTreeView.Hide;
 BackupHeaderLabel.Hide;
 CheckBoxSetVisibility(RBDelete, False);
 CheckBoxSetVisibility(RBBackup, False);
 CheckBoxSetVisibility(RBNone, False);
 CheckBoxSetVisibility(CBCleanProfile, False);
 CheckBoxSetVisibility(CBParamsRemember, False);
 //
 DescriptionMemo.Hide;
 WizardForm.ComponentsList.Hide;
 XVMList.Hide;
 BassVolumeTrackBar.Hide;
 BassVolumeLbl.Hide;
 PMODList.Hide;
 TweakerList.Hide;
 //
 ReadyLabel.Hide;
 WizardForm.ReadyMemo.Hide;
 //
 InstStatusLabel.Hide;
 InstallProgressLabel.Hide;
 WizardForm.ProgressGauge.Hide;
 InstFilenameLabel.Hide;
 InstLabel.Hide;
 //
 CheckBoxSetVisibility(CBCreateUninstallIcon, False);
 CheckBoxSetVisibility(CBGameLaunch, False);
end;

Procedure ShowObjects(CurPageID: Integer);
begin
 HideObjects();
 ImgSetVisibility(Background, True);
  #ifdef UpdateVersion
 if StartUpdate then
  case CurPageID of
   IDPPage.ID: ImgSetVisibility(UPDPageIDP, True);
  end;
 #endif
 case CurPageID of
  wpWelcome: ImgSetVisibility(wpWelcomePage, True);
   #ifdef UpdateVersion
  UpdPage.ID:
  begin
   ImgSetVisibility(UpdPagePage, True);
   UpdCheckButton.Show;
   UpdMemo.Show;
   UpdText.Show;
   AvailableUpd.Show;
   InfoAboutUpd.Show;
   DownloadStartButton.Show;
   UpdProgressGauge.Show;
  end;
  #endif
  wpInfoBefore:
  begin
   ImgSetVisibility(wpInfoBeforePage, True);
   WizardForm.InfoBeforeMemo.Show;
  end;
  wpSelectDir:
  begin
   ImgSetVisibility(wpSelectDirPage, True);
   DiskSpaceLabel.Show;
   WizardForm.DirEdit.Show;
   CheckBoxSetVisibility(CBReport, True);
   FolderTreeView.Show;
   BackupHeaderLabel.Show;
   CheckBoxSetVisibility(RBDelete, True);
   CheckBoxSetVisibility(RBBackup, True);
   CheckBoxSetVisibility(RBNone, True);
   CheckBoxSetVisibility(CBCleanProfile, True);
   CheckBoxSetVisibility(CBParamsRemember, True);
  end;
  wpSelectComponents:
  begin
   ImgSetVisibility(wpSelectComponentsPage, True);
   ImgSetVisibility(ModImage, True);
   WizardForm.ComponentsList.Show;
   DescriptionMemo.Show;
  end;
  XVMPage.ID:
  begin
   ImgSetVisibility(XVMPagePage, True);
   ImgSetVisibility(ModImage, True);
   XVMList.Show;
   DescriptionMemo.Show;
   BassVolumeTrackBar.Show;
   BassVolumeLbl.Show;
  end;
  PMODPage.ID:
  begin
   ImgSetVisibility(PMODPagePage, True);
   ImgSetVisibility(ModImage, True);
   PMODList.Show;
   DescriptionMemo.Show;
  end;
  TweakerPage.ID:
  begin
   ImgSetVisibility(TweakerPagePage, True);
   ImgSetVisibility(ModImage, True);
   TweakerList.Show;
   DescriptionMemo.Show;
  end;
  wpReady:
  begin
   ImgSetVisibility(wpReadyPage, True);
   ReadyLabel.Show;
   WizardForm.ReadyMemo.Show;
   WizardForm.NextButton.Enabled := ComponentsChecked('CheckForChecked') or XVMChecked('CheckForChecked') or PMODChecked('CheckForChecked') or TweakerChecked('CheckForChecked');
  end;
  wpInstalling:
  begin
   ImgSetVisibility(wpInstallingPage, True);
   InstStatusLabel.Show;
   InstallProgressLabel.Show;
   WizardForm.ProgressGauge.Show;
   InstFilenameLabel.Show;
   InstLabel.Show;
  end;
  wpFinished:
  begin
   ImgSetVisibility(wpFinishedPage, True);
   CheckBoxSetVisibility(CBCreateUninstallIcon, True);
   CheckBoxSetVisibility(CBGameLaunch, True);
  end;
 end;
 ImgApplyChanges(WizardForm.Handle);
end;