﻿// © Kotyarko_O, 2016 \\

[Code]
Const
 REG_ROOT = HKEY_CURRENT_USER;
 REG_KEY = 'SOFTWARE\KMP';
 CMD_GET = 'GET';
 CMD_SET = 'SET';
 CMD_GETFROMLOG = 'GETFROMLOG';
 REMEMBERED = 'Remembered';

Function SearchLog(FileName, sValue: AnsiString): Boolean;
var
 SS: TArrayOfString;
 I: Integer;
begin
 LoadStringsFromFile(FileName, SS);
 for I := 0 to GetArrayLength(SS) - 1 do
 begin
  Result := Pos(sValue, SS[I]) > 0;
  if Result then
   Exit;
 end;
end;

Procedure InstallParamsSrc(Command, LogFile: String; CheckListBox: TNewCheckListBox);
var
 I: Integer;
 RegResult: String;
begin
 case Command of
  CMD_GET:
  begin
   if RegKeyExists(REG_ROOT, REG_KEY) then
    for I := 0 to CheckListBox.Items.Count - 1 do
     if RegQueryStringValue(REG_ROOT, REG_KEY, CheckListBox.ItemCaption[I], RegResult) then
      if CheckListBox.ItemEnabled[I] then
       CheckListBox.Checked[I] := RegResult = REMEMBERED;
  end;
  CMD_GETFROMLOG:
  begin
   for I := 0 to CheckListBox.Items.Count - 1 do
    if CheckListBox.ItemEnabled[I] then
     CheckListBox.Checked[I] := SearchLog(LogFile, CheckListBox.ItemCaption[I]);
  end;
  CMD_SET:
  begin
   for I := 0 to CheckListBox.Items.Count - 1 do
    if CheckListBox.Checked[I] then
     RegWriteStringValue(REG_ROOT, REG_KEY, CheckListBox.ItemCaption[I], REMEMBERED);
  end;
 end;
end;

Procedure SetInstallParams(CurStep: TSetupStep);
begin
 if (CurStep = ssPostInstall) and CheckBoxGetChecked(CBParamsRemember) then
 begin
  if RegKeyExists(REG_ROOT, REG_KEY) then
   RegDeleteKeyIncludingSubkeys(REG_ROOT, REG_KEY);
  InstallParamsSrc(CMD_SET, '', WizardForm.ComponentsList);
  InstallParamsSrc(CMD_SET, '', XVMList);
  InstallParamsSrc(CMD_SET, '', PMODList);
  InstallParamsSrc(CMD_SET, '', TweakerList);
 end;
end;

Procedure GetInstallParams();
var
 LogFile: String;
begin
 LogFile := ExpandConstant('{src}\Log.KMP');
 if not FileExists(LogFile) then
 begin
  InstallParamsSrc(CMD_GET, '', WizardForm.ComponentsList);
  InstallParamsSrc(CMD_GET, '', XVMList);
  InstallParamsSrc(CMD_GET, '', PMODList);
  InstallParamsSrc(CMD_GET, '', TweakerList);
 end else
 try
  InstallParamsSrc(CMD_GETFROMLOG, LogFile, WizardForm.ComponentsList);
  InstallParamsSrc(CMD_GETFROMLOG, LogFile, XVMList);
  InstallParamsSrc(CMD_GETFROMLOG, LogFile, PMODList);
  InstallParamsSrc(CMD_GETFROMLOG, LogFile, TweakerList);
 finally
  MsgBoxEx(WizardForm.Handle, 'Восстановлены параметры из лог-файла установки.', '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONINFORMATION, 0, 0);
 end;
end;