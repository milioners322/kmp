﻿// © Kotyarko_O, 2016 \\

[Code]
Procedure CreateLog(CurStep: TSetupStep);
var
 FileName: String;
begin
 if CurStep = ssPostInstall then
 begin
  FileName := ExpandConstant('{app}\KMP\Analysis\Log.KMP');
  ForceDirectories(ExpandConstant('{app}\KMP\Analysis'));
  WizardForm.ReadyMemo.Lines.SaveToFile(FileName);
  SaveStringToFile(FileName, #13#10#13#10 +
   '-- {#MyAppName} for {#MyAppVersion} {#Patch}' + #13#10 +
   '-- v-{#Version} - {#CreateDate}' + #13#10 +
   '-- Install Log of - ' + GetDateTimeString('dd/mm/yyyy hh:nn:ss', '.', ':'), True);
 end;
end;