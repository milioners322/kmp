﻿[CustomMessages]
rus.IDP_FormCaption           =Скачивание дополнительных файлов
rus.IDP_FormDescription       =Пожалуйста подождите, пока инсталлятор скачает дополнительные файлы...
rus.IDP_TotalProgress         =Общий прогресс:
rus.IDP_CurrentFile           =Текущий файл:
rus.IDP_File                  =Файл:
rus.IDP_Speed                 =Скорость:
rus.IDP_Status                =Состояние:
rus.IDP_ElapsedTime           =Прошло времени:
rus.IDP_RemainingTime         =Осталось времени:
rus.IDP_DetailsButton         =Подробно
rus.IDP_HideButton            =Скрыть
rus.IDP_RetryButton           =Повтор
rus.IDP_IgnoreButton          =Пропустить
rus.IDP_KBs                   =КБ/с
rus.IDP_MBs                   =МБ/с
rus.IDP_X_of_X                =%.2f из %.2f
rus.IDP_KB                    =КБ
rus.IDP_MB                    =МБ
rus.IDP_GB                    =ГБ
rus.IDP_Initializing          =Инициализация...
rus.IDP_GettingFileInformation=Получение информации о файле...
rus.IDP_StartingDownload      =Начало загрузки...
rus.IDP_Connecting            =Соединение...
rus.IDP_Downloading           =Загрузка...
rus.IDP_DownloadComplete      =Загрузка завершена
rus.IDP_DownloadFailed        =Загрузка не удалась
rus.IDP_CannotConnect         =Невозможно соединиться
rus.IDP_CancellingDownload    =Отмена загрузки...
rus.IDP_Unknown               =Неизвестно
rus.IDP_DownloadCancelled     =Загрузка отменена
rus.IDP_RetryNext             =Проверьте ваше подключение к сети Интернет и нажмите 'Повторить' чтобы начать скачивание заново, или нажмите 'Далее' для продолжения установки.
rus.IDP_RetryCancel           =Проверьте ваше подключение к сети Интернет и нажмите 'Повторить' чтобы начать скачивание заново, или нажмите 'Отмена' чтобы прервать установку.
rus.IDP_FilesNotDownloaded    =Не удалось загрузить следующие файлы:
rus.IDP_HTTPError_X           =Ошибка HTTP %d
rus.IDP_400                   =Неверный запрос (400)
rus.IDP_401                   =Доступ запрещен (401)
rus.IDP_404                   =Файл не найден (404)
rus.IDP_407                   =Необходима авторизация прокси (407)
rus.IDP_500                   =Внутренняя ошибка сервера (500)
rus.IDP_502                   =Неправильный шлюз (502)
rus.IDP_503                   =Сервер временно недоступен (503)

[Files]
Source: "Resourses\Updater\idp.dll"; Flags: dontcopy

[Code]
Function idpNextButtonClick(IDPPage: TWizardPage): Boolean; forward;

procedure idpAddFile(url, filename: String);                     external 'idpAddFile@files:idp.dll cdecl';
procedure idpAddFileComp(url, filename, components: String);     external 'idpAddFileComp@files:idp.dll cdecl';
procedure idpAddMirror(url, mirror: String);                     external 'idpAddMirror@files:idp.dll cdecl';
procedure idpAddFtpDir(url, mask, destdir: String; recursive: Boolean); external 'idpAddFtpDir@files:idp.dll cdecl';
procedure idpAddFtpDirComp(url, mask, destdir: String; recursive: Boolean; components: String); external 'idpAddFtpDirComp@files:idp.dll cdecl';
procedure idpClearFiles();                                       external 'idpClearFiles@files:idp.dll cdecl';
function  idpFilesCount(): Integer;                              external 'idpFilesCount@files:idp.dll cdecl';
function  idpFtpDirsCount(): Integer;                            external 'idpFtpDirsCount@files:idp.dll cdecl';
function  idpFileDownloaded(url: String): Boolean;               external 'idpFileDownloaded@files:idp.dll cdecl';
function  idpFilesDownloaded(): Boolean;                         external 'idpFilesDownloaded@files:idp.dll cdecl';
function  idpDownloadFile(url, filename: String): Boolean;       external 'idpDownloadFile@files:idp.dll cdecl';
function  idpDownloadFiles(): Boolean;                           external 'idpDownloadFiles@files:idp.dll cdecl';
function  idpDownloadFilesComp(): Boolean;                       external 'idpDownloadFilesComp@files:idp.dll cdecl';
function  idpDownloadFilesCompUi(): Boolean;                     external 'idpDownloadFilesCompUi@files:idp.dll cdecl';
procedure idpStartDownload();                                    external 'idpStartDownload@files:idp.dll cdecl';
procedure idpStopDownload();                                     external 'idpStopDownload@files:idp.dll cdecl';
procedure idpSetLogin(login, password: String);                  external 'idpSetLogin@files:idp.dll cdecl';
procedure idpSetProxyMode(mode: String);                         external 'idpSetProxyMode@files:idp.dll cdecl';
procedure idpSetProxyName(name: String);                         external 'idpSetProxyName@files:idp.dll cdecl';
procedure idpSetProxyLogin(login, password: String);             external 'idpSetProxyLogin@files:idp.dll cdecl';
procedure idpConnectControl(name: String; Handle: HWND);         external 'idpConnectControl@files:idp.dll cdecl';
procedure idpAddMessage(name, message: String);                  external 'idpAddMessage@files:idp.dll cdecl';
procedure idpSetInternalOption(name, value: String);             external 'idpSetInternalOption@files:idp.dll cdecl';
procedure idpSetDetailedMode(mode: Boolean);                     external 'idpSetDetailedMode@files:idp.dll cdecl';
procedure idpSetComponents(components: String);                  external 'idpSetComponents@files:idp.dll cdecl';
procedure idpReportError();                                      external 'idpReportError@files:idp.dll cdecl';
procedure idpTrace(text: String);                                external 'idpTrace@files:idp.dll cdecl';

procedure idpAddFileSize(url, filename: String; size: DWORD);    external 'idpAddFileSize32@files:idp.dll cdecl';
procedure idpAddFileSizeComp(url, filename: String; size: DWORD; components: String); external 'idpAddFileSize32@files:idp.dll cdecl';
function idpGetFileSize(url: String; var size: DWORD): Boolean; external 'idpGetFileSize32@files:idp.dll cdecl';
function idpGetFilesSize(var size: DWORD): Boolean;             external 'idpGetFilesSize32@files:idp.dll cdecl';

type
    TIdpOptions = record
        DetailedMode   : Boolean;
        NoDetailsButton: Boolean;
        NoRetryButton  : Boolean;
        NoSkinnedButton: Boolean; //Graphical Installer
    end;

var
    IDPOptions: TIdpOptions;
        IDPPage           : TWizardPage;
        TotalProgressBar  : TNewProgressBar;
        FileProgressBar   : TNewProgressBar;
        TotalProgressLabel: TNewStaticText;
        CurrentFileLabel  : TNewStaticText;
        TotalDownloaded   : TNewStaticText;
        FileDownloaded    : TNewStaticText;
        FileNameLabel     : TNewStaticText;
        SpeedLabel        : TNewStaticText;
        StatusLabel       : TNewStaticText;
        ElapsedTimeLabel  : TNewStaticText;
        RemainingTimeLabel: TNewStaticText;
        FileName          : TNewStaticText;
        Speed             : TNewStaticText;
        Status            : TNewStaticText;
        ElapsedTime       : TNewStaticText;
        RemainingTime     : TNewStaticText;
        DetailsButton     : TNewButton;
        DetailsVisible    : Boolean;
        InvisibleButton   : TNewButton;
    BoxForProgress, BoxForStatus: TLabel;

function StrToBool(value: String): Boolean;
var s: String;
begin
    s := LowerCase(value);

    if      s = 'true'  then result := true
    else if s = 't'     then result := true
    else if s = 'yes'   then result := true
    else if s = 'y'     then result := true
    else if s = 'false' then result := false
    else if s = 'f'     then result := false
    else if s = 'no'    then result := false
    else if s = 'n'     then result := false
    else                     result := StrToInt(value) > 0;
end;

function WizardVerySilent(): Boolean;
var i: Integer;
begin
    for i := 1 to ParamCount do
    begin
        if UpperCase(ParamStr(i)) = '/VERYSILENT' then
        begin
            result := true;
            exit;
        end;
    end;
    result := false;
end;

function WizardSupressMsgBoxes(): Boolean;
var i: Integer;
begin
    for i := 1 to ParamCount do
    begin
        if UpperCase(ParamStr(i)) = '/SUPPRESSMSGBOXES' then
        begin
            result := true;
            exit;
        end;
    end;
    result := false;
end;

procedure idpSetOption(name, value: String);
var key: String;
begin
    key := LowerCase(name);

    if      key = 'detailedmode'    then IDPOptions.DetailedMode    := StrToBool(value)
    else if key = 'detailsvisible'  then IDPOptions.DetailedMode    := StrToBool(value) //alias
    else if key = 'detailsbutton'   then IDPOptions.NoDetailsButton := not StrToBool(value)
    else if key = 'skinnedbutton'   then IDPOptions.NoSkinnedButton := not StrToBool(value)
    else if key = 'retrybutton'     then 
    begin
        IDPOptions.NoRetryButton := StrToInt(value) = 0;
        idpSetInternalOption('RetryButton', value);
    end
    else
        idpSetInternalOption(name, value);
end;

procedure idpShowDetails(show: Boolean);
begin
    FileProgressBar.Visible     := show;
    CurrentFileLabel.Visible    := show;
    FileDownloaded.Visible      := show;
    FileNameLabel.Visible       := show;
    SpeedLabel.Visible          := show;
    StatusLabel.Visible         := show;
    ElapsedTimeLabel.Visible    := show;
    RemainingTimeLabel.Visible  := show;
    FileName.Visible            := show;
    Speed.Visible               := show;
    Status.Visible              := show;
    ElapsedTime.Visible         := show;
    RemainingTime.Visible       := show;
    DetailsVisible              := show;
    
    if DetailsVisible then
    begin
        DetailsButton.Caption := ExpandConstant('{cm:IDP_HideButton}');
        DetailsButton.Top := ScaleY(284);
    end
    else
    begin
        DetailsButton.Caption := ExpandConstant('{cm:IDP_DetailsButton}');
        DetailsButton.Top := ScaleY(FileProgressBar.Top);
    end;

    idpSetDetailedMode(show);
end;

procedure idpDetailsButtonClick(Sender: TObject);
begin
    idpShowDetails(not DetailsVisible);
end;

procedure idpFormActivate(IDPPage: TWizardPage);
begin
    if WizardSilent then
        idpSetOption('RetryButton', '0');
    if WizardSupressMsgBoxes then
        idpSetInternalOption('ErrorDialog', 'none');

    if not IDPOptions.NoRetryButton then
        WizardForm.BackButton.Caption := ExpandConstant('{cm:IDP_RetryButton}');
    idpShowDetails(IDPOptions.DetailedMode);
    DetailsButton.Visible := not IDPOptions.NoDetailsButton;

    idpSetComponents(WizardSelectedComponents(false));
    if WizardVerySilent then
        idpDownloadFilesComp
    else if WizardSilent then
    begin
        WizardForm.Show;
        WizardForm.Repaint;
        idpDownloadFilesCompUi;
        WizardForm.Hide;
    end
    else
        idpStartDownload;
end;

function idpShouldSkipPage(IDPPage: TWizardPage): Boolean;
begin
    idpSetComponents(WizardSelectedComponents(false));
    Result := ((idpFilesCount = 0) and (idpFtpDirsCount = 0)) or idpFilesDownloaded;
end;

function idpBackButtonClick(IDPPage: TWizardPage): Boolean;
begin
    if not IDPOptions.NoRetryButton then // Retry button clicked
    begin
        idpStartDownload;
        Result := False;
    end
    else
        Result := true;
end;

procedure idpCancelButtonClick(IDPPage: TWizardPage; var Cancel, Confirm: Boolean);
begin
    if ExitSetupMsgBox then
    begin
        Status.Caption := ExpandConstant('{cm:IDP_CancellingDownload}');
        WizardForm.Repaint;
        idpStopDownload;
        Cancel  := true;
        Confirm := false;
    end
    else
        Cancel := false;
end;

procedure idpReportErrorHelper(Sender: TObject);
begin
    idpReportError; //calling idpReportError in main thread for compatibility with VCL Styles for IS
end;

procedure idpCreateDownloadForm(PreviousPageId: Integer);
begin
    IDPPage := CreateCustomPage(PreviousPageId, '', '');

    BoxForProgress := TLabel.Create(IDPPage);
    with BoxForProgress do
    begin
     Parent := WizardForm;
     SetBounds(ScaleX(10), ScaleY(100), ScaleX(WizardForm.Width - 31), ScaleY(95));
     AutoSize := False;
     Color := clBlack;
     Transparent := False;
    end;

    TotalProgressBar := TNewProgressBar.Create(IDPPage);
    with TotalProgressBar do
    begin
        Parent := WizardForm;
        SetBounds(ScaleX(15), ScaleY(120), ScaleX(WizardForm.Width - 41), ScaleY(20));
        Min := 0;
        Max := 100;
    end;

    TotalProgressLabel := TNewStaticText.Create(IDPPage);
    with TotalProgressLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_TotalProgress}');
        SetBounds(ScaleX(15), ScaleY(104), ScaleX(200), ScaleY(15));
        AutoSize := False;
        Font.Style := [fsBold];
    end;

    CurrentFileLabel := TNewStaticText.Create(IDPPage);
    with CurrentFileLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_CurrentFile}');
        SetBounds(ScaleX(15), ScaleY(152), ScaleX(200), ScaleY(15));
        AutoSize := False;
        Font.Style := [fsBold];
    end;

    FileProgressBar := TNewProgressBar.Create(IDPPage);
    with FileProgressBar do
    begin
        Parent := WizardForm;
        SetBounds(ScaleX(TotalProgressBar.Left), ScaleY(168), ScaleX(TotalProgressBar.Width), ScaleY(TotalProgressBar.Height));
        Min := 0;
        Max := 100;
    end;

    TotalDownloaded := TNewStaticText.Create(IDPPage);
    with TotalDownloaded do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(645), ScaleY(104), ScaleX(100), ScaleY(15));
        AutoSize := False;
        Font.Style := [fsBold];
    end;

    FileDownloaded := TNewStaticText.Create(IDPPage);
    with FileDownloaded do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(645), ScaleY(152), ScaleX(100), ScaleY(15));
        AutoSize := False;
        Font.Style := [fsBold];
    end;

    BoxForStatus := TLabel.Create(IDPPage);
    with BoxForStatus do
    begin
     Parent := WizardForm;
     SetBounds(ScaleX(10), ScaleY(233), ScaleX(WizardForm.Width - 30), ScaleY(85));
     AutoSize := False;
     Color := clBlack;
     Transparent := False;
    end;

    FileNameLabel := TNewStaticText.Create(IDPPage);
    with FileNameLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_File}');
        SetBounds(ScaleX(15), ScaleY(236), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    SpeedLabel := TNewStaticText.Create(IDPPage);
    with SpeedLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_Speed}');
        SetBounds(ScaleX(15), ScaleY(252), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    StatusLabel := TNewStaticText.Create(IDPPage);
    with StatusLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_Status}');
        SetBounds(ScaleX(15), ScaleY(268), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    ElapsedTimeLabel := TNewStaticText.Create(IDPPage);
    with ElapsedTimeLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_ElapsedTime}');
        SetBounds(ScaleX(15), ScaleY(284), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    RemainingTimeLabel := TNewStaticText.Create(IDPPage);
    with RemainingTimeLabel do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_RemainingTime}');
        SetBounds(ScaleX(15), ScaleY(300), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    FileName := TNewStaticText.Create(IDPPage);
    with FileName do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(135), ScaleY(236), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    Speed := TNewStaticText.Create(IDPPage);
    with Speed do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(135), ScaleY(252), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    Status := TNewStaticText.Create(IDPPage);
    with Status do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(135), ScaleY(268), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    ElapsedTime := TNewStaticText.Create(IDPPage);
    with ElapsedTime do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(135), ScaleY(284), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    RemainingTime := TNewStaticText.Create(IDPPage);
    with RemainingTime do
    begin
        Parent := WizardForm;
        Caption := '';
        SetBounds(ScaleX(135), ScaleY(300), ScaleX(200), ScaleY(15));
        AutoSize := False;
    end;

    DetailsButton := TNewButton.Create(IDPPage);
    with DetailsButton do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('{cm:IDP_DetailsButton}');
        SetBounds(ScaleX(431), ScaleY(338), ScaleX(75), ScaleY(23));
        TabOrder := 16;
        OnClick := @idpDetailsButtonClick;
    end;
    InvisibleButton := TNewButton.Create(IDPPage);
    with InvisibleButton do
    begin
        Parent := WizardForm;
        Caption := ExpandConstant('You must not see this button');
        SetBounds(ScaleX(0), ScaleY(0), ScaleX(10), ScaleY(10));
        TabOrder := 17;
        Visible := False;
        OnClick := @idpReportErrorHelper;
    end;
  
    with IDPPage do
    begin
        OnActivate          := @idpFormActivate;
        OnShouldSkipPage    := @idpShouldSkipPage;
        OnBackButtonClick   := @idpBackButtonClick;
        OnNextButtonClick   := @idpNextButtonClick;
        OnCancelButtonClick := @idpCancelButtonClick;
    end;
end;

procedure idpConnectControls();
begin
    idpConnectControl('TotalProgressLabel', TotalProgressLabel.Handle);
    idpConnectControl('TotalProgressBar',   TotalProgressBar.Handle);
    idpConnectControl('FileProgressBar',    FileProgressBar.Handle);
    idpConnectControl('TotalDownloaded',    TotalDownloaded.Handle);
    idpConnectControl('FileDownloaded',     FileDownloaded.Handle);
    idpConnectControl('FileName',           FileName.Handle);
    idpConnectControl('Speed',              Speed.Handle);
    idpConnectControl('Status',             Status.Handle);
    idpConnectControl('ElapsedTime',        ElapsedTime.Handle);
    idpConnectControl('RemainingTime',      RemainingTime.Handle);
    idpConnectControl('InvisibleButton',    InvisibleButton.Handle);
    idpConnectControl('WizardPage',         WizardForm.Handle);
    idpConnectControl('WizardForm',         WizardForm.Handle);
    idpConnectControl('BackButton',         WizardForm.BackButton.Handle);
    idpConnectControl('NextButton',         WizardForm.NextButton.Handle);
    idpConnectControl('LabelFont',          TotalDownloaded.Font.Handle);
end;

procedure idpInitMessages();
begin
    idpAddMessage('Total progress',              ExpandConstant('{cm:IDP_TotalProgress}'));
    idpAddMessage('KB/s',                        ExpandConstant('{cm:IDP_KBs}'));
    idpAddMessage('MB/s',                        ExpandConstant('{cm:IDP_MBs}'));
    idpAddMessage('%.2f of %.2f',                ExpandConstant('{cm:IDP_X_of_X}'));
    idpAddMessage('KB',                          ExpandConstant('{cm:IDP_KB}'));
    idpAddMessage('MB',                          ExpandConstant('{cm:IDP_MB}'));
    idpAddMessage('GB',                          ExpandConstant('{cm:IDP_GB}'));
    idpAddMessage('Initializing...',             ExpandConstant('{cm:IDP_Initializing}'));
    idpAddMessage('Getting file information...', ExpandConstant('{cm:IDP_GettingFileInformation}'));
    idpAddMessage('Starting download...',        ExpandConstant('{cm:IDP_StartingDownload}'));
    idpAddMessage('Connecting...',               ExpandConstant('{cm:IDP_Connecting}'));
    idpAddMessage('Downloading...',              ExpandConstant('{cm:IDP_Downloading}'));
    idpAddMessage('Download complete',           ExpandConstant('{cm:IDP_DownloadComplete}'));
    idpAddMessage('Download failed',             ExpandConstant('{cm:IDP_DownloadFailed}'));
    idpAddMessage('Cannot connect',              ExpandConstant('{cm:IDP_CannotConnect}'));
    idpAddMessage('Unknown',                     ExpandConstant('{cm:IDP_Unknown}'));
    idpAddMessage('Download cancelled',          ExpandConstant('{cm:IDP_DownloadCancelled}'));
    idpAddMessage('HTTP error %d',               ExpandConstant('{cm:IDP_HTTPError_X}'));
    idpAddMessage('400',                         ExpandConstant('{cm:IDP_400}'));
    idpAddMessage('401',                         ExpandConstant('{cm:IDP_401}'));
    idpAddMessage('404',                         ExpandConstant('{cm:IDP_404}'));
    idpAddMessage('407',                         ExpandConstant('{cm:IDP_407}'));
    idpAddMessage('500',                         ExpandConstant('{cm:IDP_500}'));
    idpAddMessage('502',                         ExpandConstant('{cm:IDP_502}'));
    idpAddMessage('503',                         ExpandConstant('{cm:IDP_503}'));
    idpAddMessage('Retry',                       ExpandConstant('{cm:IDP_RetryButton}'));
    idpAddMessage('Ignore',                      ExpandConstant('{cm:IDP_IgnoreButton}'));
    idpAddMessage('Cancel',                      SetupMessage(msgButtonCancel));
    idpAddMessage('The following files were not downloaded:', ExpandConstant('{cm:IDP_FilesNotDownloaded}'));
    idpAddMessage('Check your connection and click ''Retry'' to try downloading the files again, or click ''Next'' to continue installing anyway.', ExpandConstant('{cm:IDP_RetryNext}'));
    idpAddMessage('Check your connection and click ''Retry'' to try downloading the files again, or click ''Cancel'' to terminate setup.', ExpandConstant('{cm:IDP_RetryCancel}'));
end;

Procedure idpDownloadAfter(PageAfterId: Integer);
begin
 idpCreateDownloadForm(PageAfterId);
 idpConnectControls();
 idpInitMessages();
end;