﻿// © Kotyarko_O, 2016 \\

[Code]
Procedure DeinitializeSetup(); forward;

Var
 NewInstallerPath, NewVersionURL: String;

Function CheckForUpdates(var Version: Integer; var UPDFile: String): Boolean;
var
 NewVersion: String;
 UpdatesURLData: String;
begin
 try
  UPDFile := ExpandConstant('{tmp}\KMPServerUpd.xml');
  Result := idpDownloadFile('{#UpdatesURL}' + 'KMPServerUpd.xml', UPDFile);
  if Result and XMLFileReadValue(UPDFile, 'KMPServerUpd.xml', UpdatesURLData) then
  begin
   XMLStringReadValue(UpdatesURLData, 'version', NewVersion);
   XMLStringReadValue(UpdatesURLData, 'downloadLink', NewVersionURL);
   StringChange(NewVersionURL, '(ver)', '(' + NewVersion + ')');
   Version := CompareVersions(Trim(NewVersion), '{#Version}');
   case Version of
    NewVersionAvailable:
    begin
     DownloadStartButton.Enabled := True;
     InfoAboutUpd.Caption := 'Доступно обновление для инсталлятора. Текущая версия: {#Version}, новая версия: ' + NewVersion + '.' + #13#10 +
      'Для загрузки новой версии нажмите кнопку «Загрузка».';
     UpdText.Caption := 'Список изменений, по сравнению с текущей версией сборки:';
     NewInstallerPath := ExpandConstant('{userdesktop}\{#MyAppExeName} (' + NewVersion + ').exe');
     idpAddFile(NewVersionURL, NewInstallerPath);
    end;
    BetaVersion:
    begin
     DownloadStartButton.Enabled := False;
     InfoAboutUpd.Caption := 'Вы используете тестовую версию сборки, так как она не указана на сервере.' + #13#10 +
      'Ваша версия: {#Version}, версия на сервере: ' + NewVersion + '.';
     UpdText.Caption := 'Подробнее:';
    end;
    VersionIsActual:
    begin
     DownloadStartButton.Enabled := False;
     InfoAboutUpd.Caption := 'Обновление не требуется. Вы используете последнюю версию сборки ({#Version}).' + #13#10 +
      'Нажмите кнопку «Далее» для продолжения установки.';
     UpdText.Caption := 'Список изменений в данной версии:';
    end;
   end;
  end;
 except
  MsgBoxEx(WizardForm.Handle, GetExceptionMessage(), '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0 ,0);
  Version := 666;
  UPDFile := '';
 end;
end;

Function idpNextButtonClick(IDPPage: TWizardPage): Boolean;
var
 ResCode: Integer;
begin
 if idpFilesCount() > 0 then
  if idpFileDownloaded(NewVersionURL) then
  begin
   Result := False;
   if Exec(NewInstallerPath, Updated, '', SW_SHOW, ewNoWait, ResCode) then
    DeinitializeSetup()
   else
   begin
    WizardForm.Hide;
    MsgBoxEx(WizardForm.Handle, 'Непредвиденная ошибка при открытии загруженного файла.' + #13#10 +
     'Result code: ' + IntToStr(ResCode),
     '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
    DeinitializeSetup();
   end;
  end else
  begin
   MsgBoxEx(WizardForm.Handle, 'Непредвиденная ошибка при загрузке обновлённой версии.' + #13#10 +
    'Пожалуйста, перезапустите установщик.',
    '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
   DeinitializeSetup();
  end;
end;