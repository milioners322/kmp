﻿// © Kotyarko_O, 2016 \\

#include "idp.iss"

[Code]
Const
 ServerFail = 1;
 VersionFail = 2;

Var
 StartUpdate: Boolean;
 UpdPage: TWizardPage;
 UpdProgressGauge: TNewProgressBar;
 UpdText, AvailableUpd, InfoAboutUpd: TLabel;
 UpdMemo: TMemo;
 UpdCheckButton, DownloadStartButton: TNewButton;

#include "ComparatorResource.iss"
#include "Comparator.iss"

Procedure MoveProgressGauge(ToPos: Integer);
var
 PBPos: Integer;
begin
 for PBPos := 0 to ToPos do
  UpdProgressGauge.Position := PBPos;
end;

Procedure ShowFail(FailType: Integer);
begin
 UpdMemo.Alignment := taCenter;
 case FailType of
  ServerFail: UpdMemo.Text := 'Подключение к серверу не удалось или отсутствует подключение к сети.' + #13#10#13#10 +
   'Проверка обновлений прервана.' + #13#10#13#10 +
   'Вы можете продолжить установку в обычном режиме, нажав кнопку «Далее».';
  VersionFail: UpdMemo.Text := 'Неизвестная версия.' + #13#10#13#10 +
   'Проверка обновлений прервана.' + #13#10#13#10 +
   'Чтобы проверить обновления снова, нажмите кнопку «Проверка».' + #13#10 +
   'Или продолжайте установку в обычном режиме, нажав кнопку «Далее».';
 end;
 UpdCheckButton.Enabled := FailType = VersionFail;
 DownloadStartButton.Enabled := False;
end;

Procedure UpdCheckButtonClick(Sender: TObject);
var
 Version: Integer;
 UPDFile, UpdText: String;
begin
 MoveProgressGauge(0);
 UpdMemo.Clear;
 try
  if CheckForUpdates(Version, UPDFile) then
  begin
   case Version of
    NewVersionAvailable, VersionIsActual:
    begin
     UpdMemo.Alignment := taLeftJustify;
     XMLFileReadValue(UPDFile, 'KMPServerUpd.xml\changes', UpdText);
     UpdMemo.Text := UpdText;
    end;
    BetaVersion:
    begin
     UpdMemo.Alignment := taCenter;
     UpdMemo.Text := '[Test]';
    end;
   else
    ShowFail(VersionFail);
   end;
  end else
   ShowFail(ServerFail);
 finally
  MoveProgressGauge(110);
 end;
end;

Procedure DownloadStartButtonClick(Sender: TObject);
begin
 StartUpdate := True;
 idpSetOption('detailedmode', '1');
 idpSetOption('detailsbutton', 'False');
 idpDownloadAfter(UpdPage.ID);
 WizardForm.NextButton.OnClick(nil);
end;

Procedure UpdaterInitialize();
begin
 UpdPage := CreateCustomPage(wpWelcome, '', '');

 UpdCheckButton := TNewButton.Create(UpdPage);
 with UpdCheckButton do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(19), Scaling(100), 100, Scaling(25));
  Caption := 'Проверка...';
  Font.Style := [fsBold];
  OnClick := @UpdCheckButtonClick;
 end;

 UpdProgressGauge := TNewProgressBar.Create(UpdPage);
 with UpdProgressGauge do
 begin
  Parent := WizardForm;
  SetBounds(UpdCheckButton.Left + UpdCheckButton.Width + Scaling(10), Scaling(102), Scaling(505), Scaling(21));
  Min := 0;
  Max := 100;
  Position := 0;
 end;

 DownloadStartButton := TNewButton.Create(UpdPage);
 with DownloadStartButton do
 begin
  Parent := WizardForm;
  SetBounds(UpdProgressGauge.Left + UpdProgressGauge.Width + Scaling(10), Scaling(100), 100, Scaling(25));
  Caption := 'Загрузка...';
  Font.Style := [fsBold];
  Enabled := False;
  OnClick := @DownloadStartButtonClick;
 end;

 AvailableUpd := TLabel.Create(UpdPage);
 with AvailableUpd do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(50), Scaling(140), 0, 0);
  AutoSize := True;
  Transparent := True;
  Caption := 'Состояние обновления:';
  Font.Size := 11;
  Font.Style := [fsBold];
 end;

 InfoAboutUpd := TLabel.Create(UpdPage);
 with InfoAboutUpd do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(60), Scaling(165), WizardForm.ClientWidth, Scaling(50));
  AutoSize := False;
  WordWrap := True;
  Transparent := True;
  Font.Size := 9;
  Caption := 'Проверка не произведена.';
 end;

 UpdText := TLabel.Create(UpdPage);
 with UpdText do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(18), InfoAboutUpd.Top + InfoAboutUpd.Height + Scaling(25), 0, 0);
  AutoSize := True;
  Transparent := True;
  Caption := 'Окно информации:';
  Font.Style := [fsBold];
  Font.Size := 11;
 end;

 UpdMemo := TMemo.Create(UpdPage);
 with UpdMemo do
 begin
  Parent := WizardForm;
  SetBounds(Scaling(18), UpdText.Top + Scaling(25), WizardForm.ClientWidth - Scaling(37), Scaling(215));
  ScrollBars := ssVertical;
  ReadOnly := True;
  Font.Size := 10;
 end;

 StartUpdate := False;
 if not CMDCheckParams(Silent) or not CMDCheckParams(Updated) then
  UpdCheckButtonClick(nil);
end;