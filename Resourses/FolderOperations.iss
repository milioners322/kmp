﻿// © Kotyarko_O, 2016 \\

[Code]
Procedure ClientFolderOperations(CurStep: TSetupStep);
var
 ResultCode: Integer;
 WOTInAppData: String;
begin
 if CurStep = ssInstall then
 try
  if CheckBoxGetChecked(RBDelete) then
  begin
   InstStatusLabel.Caption := 'Удаление ранее установленных модификаций...';
   InstFileNameLabel.Caption := ExpandConstant('{app}\res_mods\*');
   Exec(ExpandConstant('{cmd}'), '/C RMDIR /S /Q "res_mods"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
  end;
  if CheckBoxGetChecked(RBBackup) then
  begin
   InstStatusLabel.Caption := 'Создание резервной копии ранее установленных модификаций...';
   InstFileNameLabel.Caption := ExpandConstant('{app}\res_mods\*');
   if DirExists(ExpandConstant('{app}\Backup')) then
    Exec(ExpandConstant('{cmd}'), '/C RMDIR /S /Q "Backup"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   Exec(ExpandConstant('{cmd}'), '/C MOVE /Y "res_mods\*" "Backup"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
  end;
  if CheckBoxGetChecked(CBCleanProfile) then
  begin
   WOTInAppData := ExpandConstant('{userappdata}') + '\Wargaming.net\WorldOfTanks';
   DelTree(WOTInAppData + '\account_caches', True, True, True);
   DelTree(WOTInAppData + '\awesomium_cache', True, True, True);
   DelTree(WOTInAppData + '\battle_results', True, True, True);
   DelTree(WOTInAppData + '\clan_cache', True, True, True);
   DelTree(WOTInAppData + '\custom_data', True, True, True);
   DelTree(WOTInAppData + '\dossier_cache', True, True, True);
   DelTree(WOTInAppData + '\messenger_cache', True, True, True);
   DelTree(WOTInAppData + '\tutorial_cache', True, True, True);
   DeleteFile(WOTInAppData + '\preferences.xml');
  end;
  SaveStringToFile(ExpandConstant('{app}\res_mods\{#Patch}\readme.txt'), 'This folder is used for World of Tanks modifiers (mods).', False);
 except
  MsgBoxEx(WizardForm.Handle, GetExceptionMessage(), '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
 end;
end;

Procedure RestoreDirectories(CurUninstallStep: TUninstallStep);
begin
 if CurUninstallStep = usPostUninstall then
 begin
  if DirExists(ExpandConstant('{app}\Backup')) then
   if DelTree(ExpandConstant('{app}\res_mods'), True, True, True) then
    RenameFile(ExpandConstant('{app}\Backup'), ExpandConstant('{app}\res_mods'))
  else
   if not DirExists(ExpandConstant('{app}\res_mods\{#Patch}')) then
    ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
  SaveStringToFile(ExpandConstant('{app}\res_mods\{#Patch}\readme.txt'), 'This folder is used for World of Tanks modifiers (mods).', False);
 end;
end;