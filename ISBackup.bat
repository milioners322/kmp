:: © Kotyarko_O, 2016 ::

@Echo off
Color 1F

Set Own=%CD%
CD ..
Set UpperOwn=%CD%
Set DateTemp=%date:~-10%
Set BasicBackupFolder=%Own%\KMP, Basic Backup (%DateTemp%)
Set GeneralBackupFolder=%UpperOwn%\KMP, General Backup (%DateTemp%)

:Menu
  Set Answer=
  CD %Own%
  Cls
  Echo+
  Echo ------------------------------------------------------------------------------------
  Echo   ModPack Backuper                Version: Apr 07 2016
  Echo+
  Echo   Working Dir: "%Own%\ISBackup.bat"
  Echo+
  Echo   Backup-Basic Dir: "%BasicBackupFolder%"
  Echo   Backup-General Dir: "%GeneralBackupFolder%"
  Echo ------------------------------------------------------------------------------------
  Echo+
  Echo ^>Hello %USERNAME%, what do you want to do?
  Echo+
  Echo ^>Choose:
  Echo    A) Backup-Basic;
  Echo    B) Backup-General;
  Echo    Q) Quit (Exit).

:Choise
  Echo+
  Set /P Answer= ^>Select option: 
  Set Answer=%Answer:~0,1%
  If /i "%Answer%"=="Q" Exit
  If /i "%Answer%"=="A" Goto :Backup-Basic
  If /i "%Answer%"=="B" Goto :Backup-General
  Goto :Menu 

:Backup-Basic
  If EXIST "%BasicBackupFolder%" (RD /S /Q "%BasicBackupFolder%")
  MD "%BasicBackupFolder%"
  XCopy ".\FILES"				"%BasicBackupFolder%\FILES" /E /I /Y
  XCopy ".\Resourses"			"%BasicBackupFolder%\Resourses" /E /I /Y
  XCopy ".\Component_images"	"%BasicBackupFolder%\Component_images" /E /I /Y
  for %%a in ("%Own%\*.iss") do copy "%%a" "%BasicBackupFolder%\" /V
  Goto :END

:Backup-General
  If EXIST "%GeneralBackupFolder%" (RD /S /Q "%GeneralBackupFolder%")
  MD "%GeneralBackupFolder%"
  XCopy ".\*"					"%GeneralBackupFolder%" /E /I /Y
  Goto :END

:END
  Echo+
  Echo+
  @Pause
  Goto :Menu