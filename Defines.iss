﻿// © Kotyarko_O, 2016 \\

 #ifndef UNICODE
  #error Just Unicode!
#endif
 #ifndef IS_ENHANCED
  #error Необходимо расширенное издание Inno Setup (restools) для компиляции этого скрипта
#endif

#define ClientReg "{1EAC1D02-C6AC-4FA6-9A44-96258C37C812RU}_is1"
#define TestClientReg "{1EAC1D02-C6AC-4FA6-9A44-96258C37C812CT}_is1"

#define AppID "{Kotyarko-4609-4DAD-A2D4-06D4086BD394}"
#define AppMutex "KMPMutex"
#define Author "Kotyarko_O"
#define MyAppName "Kotyarko_O`s ModPack"
#define MyAppExeName "Kotyarko_O`s MP"
#define MyAppVersion "World of Tanks"
#define MyAppShortVersion "WoT"
#define UninstallShortcut "Удаление Kotyarko_O`s ModPack"

#define CreateDate GetDateTimeString('dd/mm/yyyy hh:nn:ss', '.', ':')

#define KRURL "http://www.koreanrandom.com/forum/"
#define XVMSite "http://www.modxvm.com/"
#define WOTSite "http://forum.worldoftanks.ru/topic/1429925-/"
#define ACESSite "http://aces.gg/mods/mody-dlya-worldoftanks/sborki-modov/1468-.html"

#define UpdatesURL "https://bitbucket.org/Kotyarko_O/kmp/downloads/"
