@Echo off

Set ReportFile=Logs.zip

TaskKill /T /F /IM WorldOfTanks.exe
If EXIST %ReportFile% (DEL %ReportFile%)

@Echo .Set Cabinet=ON>>make.ddf
@Echo .Set CabinetNameTemplate=%ReportFile%>make.ddf
@Echo .Set Compress=ON>>make.ddf
@Echo .Set CompressionType=MSZIP>>make.ddf
@Echo .Set DiskDirectoryTemplate=".">>make.ddf
@Echo .Set MaxCabinetSize=2500000>>make.ddf

If EXIST "Analysis\Log.KMP" (@Echo "Analysis\Log.KMP">>make.ddf)
If EXIST "..\python.log" (@Echo "..\python.log">>make.ddf)
If EXIST "..\xvm.log" (@Echo "..\xvm.log">>make.ddf)
If EXIST "..\pmod.log" (@Echo "..\pmod.log">>make.ddf)

MakeCab /F make.ddf

Del "make.ddf"
Del "setup.inf"
Del "setup.rpt"
