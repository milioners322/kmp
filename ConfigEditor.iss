﻿// © Kotyarko_O, 2016 \\

[Code]
Const
 XVM = '{app}\res_mods\configs\xvm\{#Author}\';
 PMOD = '{app}\res_mods\{#Patch}\scripts\client\gui\mods\mod_pmod\';
 GUIMODS = '{app}\res_mods\{#Patch}\scripts\client\gui\mods\';
 KMPUPD = '{app}\res_mods\configs\KMPUpdater\';

Function Editor(var Config: TStringList; DefPath, ConfigPath: String; OldValue, NewValue: AnsiString): Integer;
var
 ConfigSetStr: TArrayOfString;
 ConfigANSIStr: AnsiString;
 Temp: String;
begin
 Temp := String(ConfigANSIStr);
 try
  Config.LoadFromFile(ConfigPath);
  Temp := Config.Text;
  Result := StringChange(Temp, OldValue, NewValue);
  Config.Text := Temp;
  Config.SaveToFile(ConfigPath);
  if DefPath = XVM then
   if LoadStringsFromFile(ConfigPath, ConfigSetStr) then
    SaveStringsToUTF8File(ConfigPath, ConfigSetStr, False);
 except
  Config.Free;
  MsgBoxEx(WizardForm.Handle, GetExceptionMessage(), '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
 end;
end;

Procedure ConfigEdit(DefPath, ConfigFilePath: String; IsComponents: Boolean; OldValue, NewValue: String);
var
 Config: TStringList;
 ConfigFile: String;
begin
 if IsComponents then
 try
  Config := TStringList.Create;
  ConfigFile := ExpandConstant(DefPath + ConfigFilePath);
  ChangeConfiguratorStatus(ConfigFile);
  if Editor(Config, DefPath, ConfigFile, OldValue, NewValue) < 1 then
   MsgBoxEx(WizardForm.Handle, ConfigFile + #13#10 + OldValue + #13#10 + NewValue, 'Config value doesn`t changed', MB_ICONWARNING or MB_OK, 0, 0);
 finally
  Config.Free;
 end;
end;

Procedure ConfigurateXVM();
begin
//Автоматически
 ConfigEdit(XVM, '@xvm.xc', XVMChecked('Extended visualization mod (XVM):'), '"configVersion": ""', '"configVersion": "{#XVMver}"');
 ConfigEdit(XVM, '@xvm.xc', XVMChecked('Extended visualization mod (XVM):'), '"url": ""', '"url": "{#WoTSite}"');
 ConfigEdit(XVM, '@xvm.xc', XVMChecked('Extended visualization mod (XVM):'), '"date": ""', '"date": "{#CreateDate}"');
 ConfigEdit(XVM, '@xvm.xc', XVMChecked('Extended visualization mod (XVM):'), '"gameVersion": ""', '"gameVersion": "{#Patch}"');
 ConfigEdit(XVM, '@xvm.xc', XVMChecked('Extended visualization mod (XVM):'), '"modMinVersion": ""', '"modMinVersion": "{#XVMver}"');
//Автоматически
 ConfigEdit(XVM, 'login.xc', XVMChecked('автоматический вход в игру.'), '"autologin": false', '"autologin": true');
 ConfigEdit(XVM, 'login.xc', XVMChecked('сохранять последний выбранный сервер.'), '"saveLastServer": false', '"saveLastServer": true');
//
 ConfigEdit(XVM, 'login.xc', XVMChecked('отображать пинг серверов.'), '"enabled": false, //ping', '"enabled": true, //ping');
 ConfigEdit(XVM, 'hangar.xc', XVMChecked('отображать пинг серверов.'), '"enabled": false, //ping', '"enabled": true, //ping');
 ConfigEdit(XVM, 'hangar.xc', XVMChecked('отображать пинг серверов.'), '"enabled": true, //serverInfo', '"enabled": false, //serverInfo');
 ConfigEdit(XVM, 'clock.xc', XVMChecked('часы в ангаре.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'hangar.xc', XVMChecked('автовозврат съёмного оборудования.'), '"enableEquipAutoReturn": false', '"enableEquipAutoReturn": true');

 ConfigEdit(XVM, 'carousel.xc', XVMChecked('Карусель танков в ангаре:'), '"enabled": false', '"enabled": true');

 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('значки мастерства.'), '"enabled": false, //MarkOfMastery', '"enabled": true, //MarkOfMastery');
 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('процент побед.'), '"enabled": false, //Winrate', '"enabled": true, //Winrate');
 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('количество боёв.'), '"enabled": false, //Battles', '"enabled": true, //Battles');
 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('показатель количества отличительных отметок.'), '"enabled": false, //MarksOnGun', '"enabled": true, //MarksOnGun');
 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('показатель среднего урона за бой.'), '"enabled": false, //Damage', '"enabled": true, //Damage');
 ConfigEdit(XVM, 'carouselNormal.xc', XVMChecked('xTE - эффективность по танку.'), '"enabled": false, //XTE', '"enabled": true, //XTE');

 ConfigEdit(XVM, 'colors.xc', XVMChecked('союзники - жёлтые, противники - сиреневые.'), 'BlidnessOrNormal.ColorNormal', 'BlidnessOrNormal.ColorBlidnessOLD');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsData.xc', XVMChecked('союзники - жёлтые, противники - сиреневые.'), 'def.ColorNormal', 'def.ColorBlidnessOLD');
 ConfigEdit(XVM, 'colors.xc', XVMChecked('союзники - зелёные, противники - сиреневые.'), 'BlidnessOrNormal.ColorNormal', 'BlidnessOrNormal.ColorBlidnessNEW');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsData.xc', XVMChecked('союзники - зелёные, противники - сиреневые.'), 'def.ColorNormal', 'def.ColorBlidnessNEW');
//
 ConfigEdit(XVM, 'battleLoading.xc', XVMChecked('выделять игроков с активными сетевыми сервисами.') and not XVMChecked('отображать флаги языковых настроек игроков.'),
                 'BattleLoadingXC.NickFormat.Nothing', 'BattleLoadingXC.NickFormat.XVMUser');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('выделять игроков с активными сетевыми сервисами.') and not XVMChecked('отображать флаги языковых настроек игроков.'),
                 'StatisticFormXC.NickFormat.Nothing', 'StatisticFormXC.NickFormat.XVMUser');

 ConfigEdit(XVM, 'battleLoading.xc', XVMChecked('отображать флаги языковых настроек игроков.') and not XVMChecked('выделять игроков с активными сетевыми сервисами.'),
                 'BattleLoadingXC.NickFormat.Nothing', 'BattleLoadingXC.NickFormat.Flag');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('отображать флаги языковых настроек игроков.') and not XVMChecked('выделять игроков с активными сетевыми сервисами.'),
                 'StatisticFormXC.NickFormat.Nothing', 'StatisticFormXC.NickFormat.Flag');

 ConfigEdit(XVM, 'battleLoading.xc', XVMChecked('отображать флаги языковых настроек игроков.') and XVMChecked('выделять игроков с активными сетевыми сервисами.'),
                 'BattleLoadingXC.NickFormat.Nothing', 'BattleLoadingXC.NickFormat.FlagAndXVMUser');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('отображать флаги языковых настроек игроков.') and XVMChecked('выделять игроков с активными сетевыми сервисами.'),
                 'StatisticFormXC.NickFormat.Nothing', 'StatisticFormXC.NickFormat.FlagAndXVMUser');

 ConfigEdit(XVM, 'StatisticsTemplates.xc', XVMChecked('отображать % побед игроков.'), '{{c:t-battles}}', '{{c:winrate}}');
 ConfigEdit(XVM, 'StatisticsTemplates.xc', XVMChecked('отображать % побед игроков.'), '{{t-battles%4d|----}}', '{{winrate%d~%|--%}}');
//
 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('Иконки танков:'), '"iconAlpha": 90', '"iconAlpha": 0');
 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('Иконки танков:'), '"enabled": false, //TankIcons', '"enabled": true, //TankIcons');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('Иконки танков:'), '"enabled": false, //TankIcons', '"enabled": true, //TankIcons');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('Иконки танков:'), '"vehicleIconAlpha": 100', '"vehicleIconAlpha": 0');
 ConfigEdit(XVM, 'battle.xc', XVMChecked('от Betax, вариант 2.') or XVMChecked('от Black_Spy.') or XVMChecked('от Witblitz.') or XVMChecked('от seriych.'), '"mirroredVehicleIcons": true', '"mirroredVehicleIcons": false');
 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('от Betax, вариант 2.') or XVMChecked('от Black_Spy.') or XVMChecked('от Witblitz.') or XVMChecked('от seriych.'), '"scaleX": -1', '"scaleX": 1');
 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('от Betax, вариант 2.') or XVMChecked('от Black_Spy.') or XVMChecked('от Witblitz.') or XVMChecked('от seriych.'), '"x": -95', '"x": 0');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('от Betax, вариант 2.') or XVMChecked('от Black_Spy.') or XVMChecked('от Witblitz.') or XVMChecked('от seriych.'), '"scaleX": -1', '"scaleX": 1');
 ConfigEdit(XVM, 'statisticForm.xc', XVMChecked('от Betax, вариант 2.') or XVMChecked('от Black_Spy.') or XVMChecked('от Witblitz.') or XVMChecked('от seriych.'), '"x": -95', '"x": 0');

 ConfigEdit(XVM, 'KotoMinimap\minimap.xc', XVMChecked('Опции миникарты:'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('отображать ХП на миникарте.'), '"enabled": false, //HP', '"enabled": true, //HP');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('индикация взводных игроков на миникарте.'), '${"SquadIcons.False.', '${"SquadIcons.True.');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('индикация обнаруженных союзников на миникарте.'), '"enabled": false, //XMQPSpotted', '"enabled": true, //XMQPSpotted');
 ConfigEdit(XVM, 'KotoMinimap\minimapCircles.xc', XVMChecked('рентген-круг.'), '"enabled": false, "distance": 50', '"enabled": true, "distance": 50');
 ConfigEdit(XVM, 'KotoMinimap\minimapCircles.xc', XVMChecked('круг максимального радиуса засвета.'), '"enabled": false, "distance": 445', '"enabled": true, "distance": 445');
 ConfigEdit(XVM, 'KotoMinimap\minimapCircles.xc', XVMChecked('динамический круг обзора.'), '"enabled": false, "distance": "dynamic"', '"enabled": true, "distance": "dynamic"');
 ConfigEdit(XVM, 'KotoMinimap\minimapCircles.xc', XVMChecked('круг максимальной отрисовки.'), '"enabled": false, "distance": 564', '"enabled": "{{my-vtype-key=SPG?false|true}}", "distance": 564');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('выделять ТТ-10 отдельными маркерами.'), '${"SuperHeavyTank.Spotted.False', '${"SuperHeavyTank.Spotted.True');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('выделять ТТ-10 отдельными маркерами.'), '${"SuperHeavyTank.Lost.False', '${"SuperHeavyTank.Lost.True');
 ConfigEdit(XVM, 'KotoMinimap\minimap.xc', XVMChecked('Увеличение (центрирование) миникарты:'), '"centered": false', '"centered": true');
 ConfigEdit(XVM, 'hotkeys.xc', XVMChecked('Увеличение (центрирование) миникарты:'), '"minimapZoom": { "enabled": false', '"minimapZoom": { "enabled": true');
 ConfigEdit(XVM, 'hotkeys.xc', XVMChecked('по нажатию левого Ctrl.'), '"keyCode": 58', '"keyCode": 29');
 ConfigEdit(XVM, 'hotkeys.xc', XVMChecked('по нажатию левого Alt.'), '"keyCode": 58', '"keyCode": 56');

 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsAlt.xc', XVMChecked('Альтернативный режим миникарты:'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'hotkeys.xc', XVMChecked('Альтернативный режим миникарты:'), '"minimapAltMode": { "enabled": false', '"minimapAltMode": { "enabled": true');
 ConfigEdit(XVM, 'KotoMinimap\minimapLabelsTemplates.xc', XVMChecked('отображать никнеймы игроков.'), '"enabled": false, //Nick', '"enabled": true, //Nick');

 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('отображать ХП (по Alt).'), '"enabled": false, //HPBar', '"enabled": true, //HPBar');

 ConfigEdit(XVM, 'playersPanel.xc', XVMChecked('Отображение статуса засвета:'), '"enabled": false, //SpottedMarker', '"enabled": true, //SpottedMarker');
 ConfigEdit(XVM, 'texts.xc', XVMChecked('вариант 2 (точки).'), 'SpottedText.Default', 'SpottedText.Dots');
 ConfigEdit(XVM, 'texts.xc', XVMChecked('вариант 3 (звёздочки).'), 'SpottedText.Default', 'SpottedText.Stars');
 ConfigEdit(XVM, 'texts.xc', XVMChecked('выделять уничтоженных врагов.'), 'SpottedText.DeadSkulls.False', 'SpottedText.DeadSkulls.True');

 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('индикатор низкого запаса прочности.'), '"enabled": false, //SOS', '"enabled": true, //SOS');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('значки классности.'), '"enabled": false, //marksOfMastery', '"enabled": true, //marksOfMastery');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('индикация обнаруженных союзников в маркерах.'), '"enabled": false, //XMQPSpotted', '"enabled": true, //XMQPSpotted');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('флаги языковых настроек в маркерах.'), '"enabled": false, //PlayerFlag', '"enabled": true, //PlayerFlag');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('активность сетевых сервисов в маркерах.'), '"enabled": false, //OnlineServices', '"enabled": true, //OnlineServices');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('в стандартном режиме.'), '"enabled": false, //SquadIconNormal', '"enabled": true, //SquadIconNormal');
 ConfigEdit(XVM, 'KotoMarkers\MarkersTemplates.xc', XVMChecked('в альтернативном режиме (по Alt).'), '"enabled": false, //SquadIconExtended', '"enabled": true, //SquadIconExtended');

 ConfigEdit(XVM, 'BattleLabels\BL_WinChances.xc', XVMChecked('отображение силы команд.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_ReloadTimer.xc', XVMChecked('таймер перезарядки.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_Blocked.xc', XVMChecked('заблокированный урон.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_Assisted.xc', XVMChecked('урон, нанесённый с вашей помощью.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_RepairingTime.xc', XVMChecked('таймер ремонта модулей.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_HitLog.xc', XVMChecked('Лог нанесённого урона (хит-лог):'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'hitLog.xc', XVMChecked('простой.'), '${"History.Detailed"}', '${"History.Simple"}');
 ConfigEdit(XVM, 'BattleLabels\BL_DamageLog.xc', XVMChecked('лог полученного урона.'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_InfoPanels.xc', XVMChecked('Информационные панели:'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_InfoPanels.xc', XVMChecked('вариант от nigth_dragon_on.'), 'Templates.Empty', 'Templates.NDOFormat');
 ConfigEdit(XVM, 'BattleLabels\BL_InfoPanels.xc', XVMChecked('вариант от Kotyarko_O.'), 'Templates.Empty', 'Templates.KMPFormat');
 ConfigEdit(XVM, 'BattleLabels\BL_InfoPanels.xc', XVMChecked('стандартный полный вариант.'), 'Templates.Empty', 'Templates.FullFormat');
 ConfigEdit(XVM, 'BattleLabels\BL_InfoPanels.xc', XVMChecked('облегчённый вариант.'), 'Templates.Empty', 'Templates.LiteFormat');
 ConfigEdit(XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('Расчёт эффективности в бою:'), '"enabled": false', '"enabled": true');
 ConfigEdit(XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('по формуле WN8.'), 'Templates.Empty', 'Templates.WN8');
 ConfigEdit(XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('по рейтингу эффективности (РЭ).'), 'Templates.Empty', 'Templates.EFF');
 ConfigEdit(XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('объединённый (WN8 + РЭ).'), 'Templates.Empty', 'Templates.WN8EFF');

 ConfigEdit(XVM, 'battle.xc', XVMChecked('Модифицированные лампочки 6-го чувства:'), '"sixthSenseIcon": ""', '"sixthSenseIcon": "xvm://res/SixthSense.png"');
 ConfigEdit(XVM, 'battle.xc', XVMChecked('9-ти секундная лампочка шестого чувства.'), '"sixthSenseDuration": 2000', '"sixthSenseDuration": 9000');
//
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('Озвучка лампочки 6-го чувства (экспериментально):') or XVMChecked('Озвучка критического попадания (экспериментально):') or XVMChecked('Дополнительная озвучка (экспериментально):'), '"enabled": false', '"enabled": true');

 ConfigEdit(XVM, 'sounds.xc', XVMChecked('стандартный сигнал.'), '"sixthSense"', '"SixthSense_Standard"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('голосовое уведомление.'), '"sixthSense"', '"SixthSense_Voice"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('уведомление как на Rudy.'), '"sixthSense"', '"SixthSense_Rudy"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('сигнал с отсчётом (10 секунд).'), '"sixthSense"', '"SixthSense_SignalTimer"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('сигнал без отсчёта.'), '"sixthSense"', '"SixthSense_Signal"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('око Саурона.'), '"sixthSense"', '"SixthSense_Sauron"');

 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звонок крита.'), '/** //Crit_Damaged', '//Crit_Damaged');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звонок крита.'), '*/ //Crit_Damaged', '//Crit_Damaged');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звонок крита с голосовым уведомлением.'), '/** //Crit_Damaged_Voice', '//Crit_Damaged_Voice');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звонок крита с голосовым уведомлением.'), '*/ //Crit_Damaged_Voice', '//Crit_Damaged_Voice');

 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук обнаружения противника.'), '//"enemy_sighted_for_team"', '"enemy_sighted_for_team"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук обнаружения противника.'), '"xvm_enemySighted": ""', '"xvm_enemySighted": "Other_EnemySighted"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук пожара.'), '//"vo_fire_started"', '"vo_fire_started"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук пожара.'), '"xvm_fireAlert": ""', '"xvm_fireAlert": "Other_FireAlert"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук повреждения боеукладки.'), '//"vo_ammo_bay_damaged"', '"vo_ammo_bay_damaged"');
 ConfigEdit(XVM, 'sounds.xc', XVMChecked('звук повреждения боеукладки.'), '"xvm_ammoBay": ""', '"xvm_ammoBay": "Other_AmmoBay"');
end;

Procedure ConfiguratePMOD();
begin
 ConfigEdit(PMOD, 'noBinoculars.json', PMODChecked('Убрать затемнение в снайперском режиме.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'noDynamic.json', PMODChecked('Отключить эффекты динамичесой камеры.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'horizontalStabilizer.json', PMODChecked('Включить горизонтальную стабилизацию.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'videomode.json', PMODChecked('Свободная камера (в реплеях).'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'zoomDistance.json', PMODChecked('Максимальное отдаление (командирская камера).'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'cameraOptions.json', PMODChecked('Отключить красное моргание при попадании по вам.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'cameraOptions.json', PMODChecked('Отключить красное моргание при попадании по вам.'), '"noFlashBang": false', '"noFlashBang": true');
 ConfigEdit(PMOD, 'TDBrakeRemover.json', PMODChecked('Отключение ручного тормоза на ПТ-САУ.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'TDBrakeRemover.json', PMODChecked('Отключение ручного тормоза на ПТ-САУ.'), '"TDSniperMovementMode": "custom"', '"TDSniperMovementMode": "full"');
 ConfigEdit(PMOD, 'reducedArmor.json', PMODChecked('Индикатор бронепробиваемости.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'messagesCleaner.json', PMODChecked('Фильтр сообщений в ангаре.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'messagesCleaner.json', PMODChecked('Фильтр сообщений в ангаре.'), '"popUpMessages": false', '"popUpMessages": true');
 ConfigEdit(PMOD, 'messagesCleaner.json', PMODChecked('Фильтр сообщений в ангаре.'), '"systemLog": false', '"systemLog": true');

 ConfigEdit(PMOD, 'oldServerCrosshair.json', PMODChecked('Стандартный вариант маркера.') or PMODChecked('Модернизированный вариант маркера.'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'oldServerCrosshair.json', PMODChecked('Модернизированный вариант маркера.'), 'gun_marker_standart', 'gun_marker_custom');

 ConfigEdit(PMOD, 'zoomX.json', PMODChecked('Увеличение зума снайперского прицела:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'zoomX.json', PMODChecked('до х32.'), '"zoomXSteps": [2, 4, 8, 16]', '"zoomXSteps": [2, 4, 6, 8, 10, 16, 20, 24, 32]');
 ConfigEdit(PMOD, 'zoomX.json', PMODChecked('до х50.'), '"zoomXSteps": [2, 4, 8, 16]', '"zoomXSteps": [2, 4, 8, 12, 20, 28, 34, 40, 50]');

 ConfigEdit(PMOD, 'defaultZoom.json', PMODChecked('Стартовый зум снайперского прицела:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'defaultZoom.json', PMODChecked('2-х.'), '"defaultZoomValue": 4', '"defaultZoomValue": 2');
 ConfigEdit(PMOD, 'defaultZoom.json', PMODChecked('6-х.'), '"defaultZoomValue": 4', '"defaultZoomValue": 6');
 ConfigEdit(PMOD, 'defaultZoom.json', PMODChecked('24-х.'), '"defaultZoomValue": 4', '"defaultZoomValue": 24');

 ConfigEdit(PMOD, 'battleGui.json', PMODChecked('ХП команд:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'battleGui.json', PMODChecked('оформление от Armagomen.'), '"displayDamage": true', '"displayDamage": false');
 ConfigEdit(PMOD, 'battleGui.json', PMODChecked('оформление от Locastan.'), '"skin": "armagomen"', '"skin": "locastan"');
 ConfigEdit(PMOD, 'battleGui.json', PMODChecked('оформление Minimal.'), '"skin": "armagomen"', '"skin": "minimal"');

 ConfigEdit(PMOD, 'battleChat.json', PMODChecked('Сессионная статистика:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'sessionStatistic.json', PMODChecked('Сессионная статистика:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'sessionStatistic.json', PMODChecked('Сессионная статистика:'), '"autoReset": false', '"autoReset": true');
 ConfigEdit(PMOD, 'systemMessages.json', PMODChecked('Сессионная статистика:'), '"enable": false', '"enable": true');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от Kotyarko_O.'), 'default', 'Kotyarko_O');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от vlad_cs_sr.'), 'default', 'vlad_cs_sr');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от Meddio.'), 'default', 'Meddio');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от Xxx_mutant.'), 'default', 'XXX_MUTANT');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от demon2597.'), 'default', 'demon2597');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от Antoshkaaa.'), 'default', 'Antoshkaaa');
 ConfigEdit(PMOD, 'skins\skinLoader.json', PMODChecked('вариант от Armagomen.'), 'default', 'Armagomen');
end;

Procedure ConfigurateKMPUpdater();
begin
 ConfigEdit(KMPUPD, 'KMPClientUpd.json', True, '"0.0"', '"{#Version}"');
end;

Procedure ConfigurateEdgeDetectLite();
begin
 ConfigEdit(GUIMODS, 'mod_EdgeDetectLite.xml', IsComponentSelected('KMP\CONTOURS\WHITE'), '<color>255 18 7 255</color>', '<color>255 255 255 255</color>');
 ConfigEdit(GUIMODS, 'mod_EdgeDetectLite.xml', IsComponentSelected('KMP\CONTOURS\YELLOW'), '<color>255 18 7 255</color>', '<color>207 146 49 255</color>');
 ConfigEdit(GUIMODS, 'mod_EdgeDetectLite.xml', IsComponentSelected('KMP\CONTOURS\BLUE'), '<color>255 18 7 255</color>', '<color>130 120 253 255</color>');
end;

Procedure ConfiguratorInit(CurStep: TSetupStep);
begin
 if CurStep = ssPostInstall then
 begin
  ConfigurateXVM();
  ConfiguratePMOD();
  ConfigurateKMPUpdater();
  ConfigurateEdgeDetectLite();
 end;
end;