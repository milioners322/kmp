**Вики \ Wiki: https://bitbucket.org/Kotyarko_O/kmp/wiki/Home**

**Обсуждение: http://forum.worldoftanks.ru/topic/1429925-/**

**Дополнительные ресурсы:**

* [Apache License](http://www.apache.org/licenses/)
* [jrsoftware.org](http://jrsoftware.org/)
* [restools](http://restools.hanzify.org/)
* [Inno Download Plugin](https://bitbucket.org/mitrich_k/inno-download-plugin)
* [VCL-Styles](https://github.com/RRUZ/vcl-styles-plugins)
* [Bass](http://www.un4seen.com/)
* [iCatalyst](https://github.com/lorents17/iCatalyst)

**Автор: Kotyarko_O.**
**Copyright 2015-2016 The Apache Software Foundation.**
