﻿// © Kotyarko_O, 2016 \\

[Setup]
AppId={{#AppID}
AppMutex={#AppMutex}
AppName={#MyAppName} ({#Version})
AppVersion={#MyAppShortVersion} ({#Patch})
AppPublisher={#Author}
AppPublisherURL={#WoTSite}
AppSupportURL={#WoTSite}
AppUpdatesURL={#WoTSite}
DefaultDirName={code:GetInstallDir}
AppendDefaultDirName=no
DirExistsWarning=no
DefaultGroupName={#MyAppName} ({#Version})
DisableProgramGroupPage=yes
OutputDir=BUILDED
OutputBaseFilename={#MyAppExeName} ({#Version})
SetupIconFile=FILES\KMPLogo.ico
InfoBeforeFile=FILES\ChangeLog.txt
AppComments={#Author}
VersionInfoVersion={#Version}
VersionInfoTextVersion={#Version}
VersionInfoDescription={#MyAppName} for {#MyAppVersion}
AppCopyright=© {#Author} 2014-2016
UninstallLogMode=new
UninstallDisplayIcon={app}\KMP\unins000.exe
UninstallFilesDir={app}\KMP
UninstallDisplayName={#MyAppName} ({#Version})
UsePreviousSetupType=no
PrivilegesRequired=poweruser
ShowLanguageDialog=no
 #ifdef Compress
Compression=lzma2
SolidCompression=yes
LZMAUseSeparateProcess=yes
LZMADictionarySize=262144
LZMABlockSize=262144
LZMANumBlockThreads=1
#endif

[Languages]
Name: rus; MessagesFile: "Resourses\Russian.isl"