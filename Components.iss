﻿// © Kotyarko_O, 2016 \\

[Components]
Name: KMP; Description: {#MyAppName}; Flags: disablenouninstallwarning
//Прицелы\\                      
Name: KMP\SIGHTS; Description: Прицелы:; Flags: disablenouninstallwarning
 Name: KMP\SIGHTS\STDEXT; Description: улучшенный стандартный, от AtotIK.; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\DELLUX; Description: прицел от Dellux.; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\WHITESTYLE; Description: прицел «White style».; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\KRAN; Description: прицел от КРАН`а.; Flags: fixed exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\JP; Description: прицел как у Jove.; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\J; Description: прицел от Jimbo.; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\KO; Description: прицел как у Кирилла Орешкина.; Flags: fixed exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\DM; Description: прицел «Дамоклов меч».; Flags: fixed exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\T; Description: прицел «Taipan».; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\MM; Description: прицел от MeltyMap.; Flags: fixed exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\SPCTR; Description: прицел от Spectr20.; Flags: exclusive disablenouninstallwarning
 Name: KMP\SIGHTS\HARPOON; Description: прицел «Harpoon».; Flags: exclusive disablenouninstallwarning
//УГН\\
Name: KMP\UGN; Description: Углы горизонтальной наводки:; Flags: disablenouninstallwarning
 Name: KMP\UGN\U; Description: углы.; Flags: exclusive disablenouninstallwarning
 Name: KMP\UGN\O; Description: Octagon.; Flags: exclusive disablenouninstallwarning
 Name: KMP\UGN\S; Description: скобки.; Flags: exclusive disablenouninstallwarning
 Name: KMP\UGN\PS; Description: прямые скобки.; Flags: exclusive disablenouninstallwarning
//Контуры танков\\
Name: KMP\CONTOURS; Description: Контуры обведения противников:; Flags: disablenouninstallwarning
 Name: KMP\CONTOURS\WHITE; Description: белый.; ExtraDiskSpaceRequired: 1000; Flags: exclusive disablenouninstallwarning
 Name: KMP\CONTOURS\YELLOW; Description: жёлтый.; ExtraDiskSpaceRequired: 1000; Flags: exclusive disablenouninstallwarning
 Name: KMP\CONTOURS\BLUE; Description: синий.; ExtraDiskSpaceRequired: 1000; Flags: exclusive disablenouninstallwarning
//Ангарные\\
Name: KMP\HANGAR; Description: Ангарные улучшения:; Flags: disablenouninstallwarning
 Name: KMP\HANGAR\WGFM; Description: Радио в ангаре (Wargaming FM).; Flags: disablenouninstallwarning
 Name: KMP\HANGAR\HM; Description: «Менеджер ангаров».; Flags: disablenouninstallwarning
 Name: KMP\HANGAR\RM; Description: «Менеджер реплеев».; Flags: disablenouninstallwarning
 Name: KMP\HANGAR\CS; Description: Подробное описание умений и навыков экипажа.; Flags: disablenouninstallwarning
 Name: KMP\HANGAR\VT; Description: Вертикальное дерево исследования.; Flags: fixed disablenouninstallwarning
 Name: KMP\HANGAR\GPT; Description: Золотые иконки премиум-танков в карусели.; Flags: disablenouninstallwarning
//MCTCreator\\
Name: KMP\MCT; Description: Изменение игровых текстур (MCTCreator):; Flags: disablenouninstallwarning
 Name: KMP\MCT\DPT; Description: изменение текстур уничтоженной техники.; ExtraDiskSpaceRequired: 38207488; Flags: disablenouninstallwarning
 Name: KMP\MCT\CT; Description: изменение текстур сбитых гусениц\траков.; ExtraDiskSpaceRequired: 6586368; Flags: disablenouninstallwarning
//Разное\\
Name: KMP\OTHER; Description: Дополнительные модификации:; Flags: disablenouninstallwarning
 Name: KMP\OTHER\BA; Description: Battle Assistant - «САУ здорового человека».; Flags: disablenouninstallwarning
 Name: KMP\OTHER\TCP; Description: Минимизированная панель счёта в бою.; Flags: disablenouninstallwarning
 Name: KMP\OTHER\OBS; Description: «Я в засвете!»:; Flags: dontinheritcheck disablenouninstallwarning
  Name: KMP\OTHER\OBS\6; Description: активируется при 6-ти оставшихся союзниках.; Flags: exclusive disablenouninstallwarning
  Name: KMP\OTHER\OBS\15; Description: активно с начала и до окончания боя.; Flags: exclusive disablenouninstallwarning
 Name: KMP\OTHER\SH; Description: «Safe-shot»:; Flags: disablenouninstallwarning
  Name: KMP\OTHER\SH\DS; Description: + блокировка выстрелов по уничтоженным.; Flags: disablenouninstallwarning
 Name: KMP\OTHER\TYLY; Description: «Тылы».; Flags: disablenouninstallwarning
 Name: KMP\OTHER\ST; Description: «Стволик хаоса».; Flags: disablenouninstallwarning
 Name: KMP\OTHER\PM; Description: Пейнтбол-мод.; Flags: disablenouninstallwarning
 Name: KMP\OTHER\MT; Description: Направление обзора противника на миникарте.; Flags: fixed disablenouninstallwarning
 Name: KMP\OTHER\KI; Description: Модифицированные иконки чата и киллога.; Flags: disablenouninstallwarning
 Name: KMP\OTHER\ANTITOXICITY; Description: «Антитоксичность» - фильтр спама в боевом чате.; Flags: disablenouninstallwarning
 Name: KMP\OTHER\CS; Description: Случайные камуфляжи в бою и ангаре.; Flags: disablenouninstallwarning

[Files]
 #ifdef FOR_TESTING
Source: "MODS\_UPDATER\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..');
//Прицелы\\
 Source: "MODS\SIGHTS\STANDARD_EXT\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\STDEXT;
 Source: "MODS\SIGHTS\DELLUX\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\DELLUX;
 Source: "MODS\SIGHTS\WHITE_STYLE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\WHITESTYLE;
 Source: "MODS\SIGHTS\KRAN\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\KRAN;
 Source: "MODS\SIGHTS\JOVE`S\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\JP;
 Source: "MODS\SIGHTS\JIMBO\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\J;
 Source: "MODS\SIGHTS\ORESHKIN\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\KO;
 Source: "MODS\SIGHTS\DAMOKLOV_MECH\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\DM;
 Source: "MODS\SIGHTS\TAIPAN\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\T;
 Source: "MODS\SIGHTS\MELTY_MAP\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\MM;
 Source: "MODS\SIGHTS\SPECTR20\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\SPCTR;
 Source: "MODS\SIGHTS\HARPOON\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\SIGHTS\HARPOON;
//УГН\\
Source: "MODS\UGN\BASIS\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\UGN;
 Source: "MODS\UGN\UGLI\*"; DestDir: "{app}\res_mods\{#Patch}\gui\flash"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\UGN\U;
 Source: "MODS\UGN\OCTAGON\*"; DestDir: "{app}\res_mods\{#Patch}\gui\flash"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\UGN\O;
 Source: "MODS\UGN\SKOBKI\*"; DestDir: "{app}\res_mods\{#Patch}\gui\flash"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\UGN\S;
 Source: "MODS\UGN\PRYAMIE_SKOBKI\*"; DestDir: "{app}\res_mods\{#Patch}\gui\flash"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\UGN\PS;
//Контуры танков\\
 Source: "MODS\CONTOURS\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\CONTOURS;
//Ангарные улучения\\
Source: "MODS\HANGAR\WARGAMING_FM\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\WGFM;
Source: "MODS\HANGAR\HANGAR_MANAGER\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\HM;
Source: "MODS\HANGAR\REPLAYS_MANAGER\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\RM;
Source: "MODS\HANGAR\CREW_SKILLS\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\CS;
Source: "MODS\HANGAR\VERT_TREE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\VT;
Source: "MODS\HANGAR\GOLD_PREM_TANKS\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\HANGAR\GPT;
//MCTCreator\\
Source: "MODS\MCTCREATOR\*.cfg"; DestDir: "{app}\KMP\MCTCreator"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Изменение игровых текстур..'); Components: KMP\MCT;
Source: "MODS\MCTCREATOR\MCTCreator.exe"; DestDir: "{app}\KMP\MCTCreator"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Изменение игровых текстур..'); AfterInstall: MCTCreatorLaunch(); Components: KMP\MCT;
//Разное\\
Source: "MODS\OTHER\BATTLE_ASSISTANT\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\BA;
Source: "MODS\OTHER\TANKS_COUNT_PANEL\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\TCP;
Source: "MODS\OTHER\OBSERVED\BASIS\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\OBS;
 Source: "MODS\OTHER\OBSERVED\6\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\OBS\6;
 Source: "MODS\OTHER\OBSERVED\15\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\OBS\15;
Source: "MODS\OTHER\SAFE_SHOT\BASE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\SH;
 Source: "MODS\OTHER\SAFE_SHOT\DEAD_SHOT\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\SH\DS;
Source: "MODS\OTHER\TYLY\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\TYLY;
Source: "MODS\OTHER\SERVER_TURRET\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\ST;
Source: "MODS\OTHER\PAINTBALL\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\PM;
Source: "MODS\OTHER\MINIMAP_TANKVIEW\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\MT;
Source: "MODS\OTHER\KILLOG_ICONS\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\KI;
Source: "MODS\OTHER\ANTITOXICITY\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\ANTITOXICITY;
Source: "MODS\OTHER\CAMO_SELECTOR\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Распаковка файлов..'); Components: KMP\OTHER\CS;

/////////////////////////////////////XVM
Source: "MODS\XVM\XVM_BASE\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('Extended visualization mod (XVM):');
Source: "MODS\XVM\XVM_KOTO_CONFIG\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('Extended visualization mod (XVM):');

Source: "MODS\XVM\TANK_ICONS\BETAX_COLORED_CLEAR\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('от Betax, вариант 1.');
Source: "MODS\XVM\TANK_ICONS\BETAX_STANDARD_WHITE\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('от Betax, вариант 2.');
Source: "MODS\XVM\TANK_ICONS\XOBOTYI\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('в стиле xobotyi, от galagan.');
Source: "MODS\XVM\TANK_ICONS\BLACKSPY\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('от Black_Spy.');
Source: "MODS\XVM\TANK_ICONS\WITBLITZ\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('от Witblitz.');
Source: "MODS\XVM\TANK_ICONS\SERIYCH\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('от seriych.');

Source: "MODS\XVM\SIXTH_SENSE_ICONS\XVMStandard.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('стандартная XVM.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Lol.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('lol.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Pedobear.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('педобир.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Triangle.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('красный стоп-сигнал.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Lamp_in_circle.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('лампа в кругу.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Lamp.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('простая лампа.');
Source: "MODS\XVM\SIXTH_SENSE_ICONS\Eye.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: ChangeInstallStatus('Установка XVM-конфига..'); Check: XVMChecked('око.');

/////////////////////////////////////PMOD
Source: "MODS\PMOD\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка PMOD..'); Check: PMODChecked('Комплексная модификация PMOD:');

/////////////////////////////////////Wot-Tweaker
Source: "MODS\TWEAKS\EMBLEMS_OFF\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Отключить загрузку эмблем.');
Source: "MODS\TWEAKS\MULTICORE_OFF\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Отключить многоядерность.');
Source: "MODS\TWEAKS\FOG\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать туман (дымку) на картах.');
Source: "MODS\TWEAKS\TANK_DESTROY_SMOKE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать дым от уничтоженной техники.');
Source: "MODS\TWEAKS\TANK_SHOOT_SMOKE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать дым и пламя при выстреле.');
Source: "MODS\TWEAKS\TANK_SMOKE\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать дым из выхлопной трубы техники.');
Source: "MODS\TWEAKS\SKYBOXES\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать облака.');
Source: "MODS\TWEAKS\TREES\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать движение деревьев.');
Source: "MODS\TWEAKS\TANK_DESTROY\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать эффект уничтожения техники.');
Source: "MODS\TWEAKS\ENVIRONMENT\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать эффекты взрыва снарядов и попадания в объекты.');
Source: "MODS\TWEAKS\TANK_HIT\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать эффект попадания в танк.');
Source: "MODS\TWEAKS\INTERIOR\*"; DestDir: "{app}\res_mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: ChangeInstallStatus('Установка Wot-Tweaker..'); Check: TweakerChecked('Убрать эффект проявления погоды и дым от объектов.');
#endif
         
[Code]
Procedure MCTCreatorLaunch();
var
 ResCode: Integer;
 Params: String;
begin
 ResCode := 666;
 if FileExists(ExpandConstant('{app}\KMP\MCTCreator\MCTCreator.exe')) then
 begin
  if IsComponentSelected('KMP\MCT') then
   Params := '/wot-path "' + ExpandConstant('{app}') + '" /operational-mode=0';
  if IsComponentSelected('KMP\MCT\DPT') then
   Params := Params + ' /DPT-mod "#FFFFFF"';
  if IsComponentSelected('KMP\MCT\CT') then
   Params := Params + ' /CT-mod "#FFFFFF"';
  Exec(ExpandConstant('{app}\KMP\MCTCreator\MCTCreator.exe'), Params, ExpandConstant('{app}\KMP\MCTCreator'), SW_SHOW, ewWaitUntilTerminated, ResCode);
  if ResCode <> 0 then
   MsgBoxEx(WizardForm.Handle, 'Непредвиденная ошибка.' + #13#10 + 'Result code is: ' + IntToStr(ResCode) + '.', '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
 end else
  MsgBoxEx(WizardForm.Handle, 'Файлы "\KMP\MCTCreator\*" не найдены.' + #13#10 + 'Пожалуйста, перезапустите установщик.', '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
end;