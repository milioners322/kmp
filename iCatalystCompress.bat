@Echo on
@Pause

Call Resourses\iCatalyst\iCatalyst.bat /png:2 /jpg:2 "/outdir:false" ".\FILES\Images_Components\*"

If EXIST "MODS\XVM\TANK_ICONS" (
	Call Resourses\iCatalyst\iCatalyst.bat /png:2 "/outdir:false" ".\MODS\XVM\TANK_ICONS\*"
)

@Pause
