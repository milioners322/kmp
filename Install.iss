﻿// © Kotyarko_O, 2016 \\

#define Patch "0.9.16"
#define Version "9.16.0.2"
#define XVMver "6.4.7.1"

 #if FindFirst("MODS\_UPDATER", faDirectory)
#define FOR_TESTING
#endif

#define Compress
#define VCL "Windows10Dark"
#define SearchGameFiles
#define CheckForRun
#define UpdateVersion

#include "Defines.iss"
#include "Setup.iss"
#include "Resourses\CMDCheckParams.iss"
#include "Resourses\botva2.iss"
#include "Resourses\Scaling.iss"
#include "Resourses\XML.iss"
 #ifdef VCL
#include "Resourses\VCL\VCLStyle.iss"
#endif
 #ifdef UpdateVersion
#include "Resourses\Updater\Updater.iss"
#endif
#include "Resourses\Window.iss"
#include "Resourses\CreateObjects.iss"
#include "Resourses\Log.iss"
#include "Resourses\CompSound.iss"
#include "Resourses\ImgsDescs.iss"
#include "Resourses\Interface.iss"
#include "Resourses\FolderOperations.iss"
#include "Resourses\ReadyMemo.iss"
#include "Resourses\ParamsRememberer.iss"
#include "ConfigEditor.iss"
#include "Components.iss"

[Files]
Source: "FILES\ConsoleWorks\*"; DestDir: "{app}\KMP"; Flags: ignoreversion
Source: "FILES\Fonts\*"; DestDir: "{app}\KMP\Fonts"; Flags: ignoreversion
Source: "FILES\Fonts\Cuprum-Regular.ttf"; DestDir: "{fonts}"; FontInstall: "Cuprum"; Flags: uninsneveruninstall
Source: "FILES\Fonts\Cuprum-Bold.ttf"; DestDir: "{fonts}"; FontInstall: "Cuprum"; Flags: uninsneveruninstall
Source: "FILES\Fonts\Micra.ttf"; DestDir: "{fonts}"; FontInstall: "Micra"; Flags: onlyifdoesntexist uninsneveruninstall
Source: "FILES\Fonts\Archangelsk.ttf"; DestDir: "{fonts}"; FontInstall: "Archangelsk"; Flags: onlyifdoesntexist uninsneveruninstall

[InstallDelete]
Type: filesandordirs; Name: "{app}\KMP";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\mod_pmod";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\PYmodsGUI";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\wgfm\data";
Type: files; Name: "{app}\res_mods\configs\KMPUpdater\KMPClientUpd.xml";
Type: files; Name: "{app}\python.log";
Type: files; Name: "{app}\xvm.log";
Type: files; Name: "{app}\pmod.log";

[UninstallDelete]
Type: filesandordirs; Name: "{app}\KMP";
Type: files; Name: "{app}\python.log";
Type: files; Name: "{app}\xvm.log";
Type: files; Name: "{app}\pmod.log";
Type: filesandordirs; Name: "{app}\res_mods\configs";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\mod_pmod";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\PYmodsGUI";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\rmanager\database";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts\client\gui\mods\wgfm\data";
Type: filesandordirs; Name: "{app}\res_mods\mods\shared_resources\xvm\cache";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\vehicles";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\content";
Type: dirifempty; Name: "{app}\res_mods\{#Patch}\scripts";
Type: dirifempty; Name: "{app}\res_mods\mods";

[Code]
Procedure ExitProcess(ExitCode: Integer); external 'ExitProcess@kernel32.dll stdcall';

Function GetInstallDir(Path: String): String;
var
 DirPath: String;
 DP1, DP2, DPXVM, DPTweaker: Boolean;
begin
 DP1 := RegQueryStringValue(HKLM32 or HKLM64, 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#ClientReg}', 'InstallLocation', DirPath);
 DP2 := RegQueryStringValue(HKCU32 or HKCU64, 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#ClientReg}', 'InstallLocation', DirPath);
 DPXVM := RegQueryStringValue(HKLM32 or HKLM64, 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{2865cd27-6b8b-4413-8272-cd968f316050}_is1', 'InstallLocation', DirPath);
 DPTweaker := RegQueryStringValue(HKCU32 or HKCU64, 'Software\WoTTweakerPlus', 'Folder', DirPath);
 if (DP1 or DP2 or DPXVM or DPTweaker) and (DirPath <> '') then
  Result := DirPath
 else
  Result := ExpandConstant('{pf}\World_of_Tanks\');
end;

Function InitializeSetup(): Boolean;
begin
 Result := True;
  #ifdef VCL
 VCLInitializeSetup();
 #endif
end;

 #ifdef CheckForRun
Function CheckForGameRun(CMDParam: Boolean): Boolean;
var
 Launcher, Client: Boolean;
 ResultCode: Integer;
begin
 if not CMDParam then
 begin
  Launcher := FindWindowByWindowName('World of Tanks (Online Game)') <> 0;
  Client := FindWindowByWindowName('WoT Client') <> 0;
  if Launcher or Client then
  begin
   if MsgBoxEx(0, 'Обнаружено запущенное приложение {#MyAppVersion}. Перед продолжением требуется закрыть все экземпляры приложения.' + #13#10 + 'Закрыть игру?',
    'Внимание!', MB_YESNO + MB_DEFBUTTON1 or MB_ICONWARNING, 0, 0) = IDYES then
   begin
    if Launcher then
     Exec(ExpandConstant('{cmd}'), '/C TASKKILL /F /IM "WoTLauncher.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
    if Client then
     Exec(ExpandConstant('{cmd}'), '/C TASKKILL /F /IM "WorldOfTanks.exe"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
    case ResultCode of
     0: Result := True;
     128: Result := True;
    else
     Result := False;
    end;
   end else
    Result := False;
  end else
   Result := True;
  if not Result then
   Abort();
 end;
end;
#endif

Function NextButtonClick(CurPageID: Integer): Boolean;
var
 PatchVersion: String;
begin
 Result := True;
 case CurPageID of
   #ifdef CheckForRun
  wpWelcome: CheckForGameRun(CMDCheckParams(NoCheckForRun));
  #endif
   #ifdef SearchGameFiles
  wpSelectDir:
  begin
   if not CMDCheckParams(NoSearchGameFiles) then
    if not (FileExists(ExpandConstant('{app}\WOTLauncher.exe')) and FileExists(ExpandConstant('{app}\version.xml'))) then
    begin
     MsgBoxEx(WizardForm.Handle, 'Выбрана неверная директория для установки.' + #13#10 + 'Пожалуйста, проверьте правильность пути до папки с игрой и попробуйте снова.',
      'Внимание!', MB_OK or MB_ICONWARNING, 0, 0);
     Result := False;
    end else
    begin
     XMLFileReadValue(ExpandConstant('{app}\version.xml'), 'version.xml\version', PatchVersion);
     Delete(PatchVersion, Pos('v', PatchVersion), 2);
     Delete(PatchVersion, Pos('#', PatchVersion) - 1, 10);
     if CompareStr(PatchVersion, '{#Patch}') <> 0 then
     begin
      MsgBoxEx(WizardForm.Handle, 'Неподходящая версия установленного клиента: ' + PatchVersion + #13#10 + 'Сборка предназначена для патча {#Patch}',
       'Внимание!', MB_OK or MB_ICONWARNING, 0, 0);
      Result := False;
     end;
    end;
  end;
  #endif
 end;
end;

Procedure InitializeWizard();
begin
 if not CMDCheckParams(NoCheckForMutex) then
  CreateMutex('{#AppMutex}');
 DisplayScale();
 Window();
 CreatePagesImages();
  #ifdef UpdateVersion
 UpdaterInitialize();
 #endif
 CreateObjects();
 InitializeSounds();
 InitializeImgsDescs();
 GetInstallParams();
end;

Function UpdateReadyMemo(Space, NewLine, MemoUserInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
begin
 Result := _UpdateReadyMemo(Space, NewLine, MemoDirInfo);
end;

Procedure CurPageChanged(CurPageID: Integer);
begin
 ShowObjects(CurPageID);
end;

Procedure CurStepChanged(CurStep: TSetupStep);
begin
 ClientFolderOperations(CurStep);
 SetInstallParams(CurStep);
  #ifdef FOR_TESTING
 ConfiguratorInit(CurStep);
 #endif
 LaunchGame(CurStep);
 CreateLog(CurStep);
end;

Procedure DeinitializeSetup();
begin
 DeinitializeSounds();
 gdipShutdown();
  #ifdef VCL
 UnLoadVCLStyles();
 #endif
 DelTree(ExpandConstant('{tmp}'), True, True, True);
 ExitProcess(0);
end;

Function InitializeUninstall(): Boolean;
begin
 Result := True;
  #ifdef CheckForRun
 CheckForGameRun(False);
 #endif
end;

Procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
 RestoreDirectories(CurUninstallStep);
 if FileExists(ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk')) then
  DeleteFile(ExpandConstant('{userdesktop}\{#UninstallShortcut}.lnk'));
end;