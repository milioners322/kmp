::It`s necessarry to have environment variable "WinRAR"
::which contains full path to WinRAR.exe application

@Echo off

If Not DEFINED WinRAR (
	@Echo "Error: The environment variable WinRAR doesn`t exist."
	GOTO:_Exit
) else (
	If Not EXIST ".\*xvm*.zip" (
		@Echo "Error: Needed archive ('latest_xvm'.zip or 'xvm-*version*'.zip) doesn`t exist in: %~dp0"
		GOTO:_Exit
	) else (
		GOTO:_Unpack
	)
)

:_Unpack
	Set WinRAR
	
	@Echo "Info: Start unpacking..."
	
	RD /S /Q ".\XVM_BASE"
	
	%WinRAR% X ".\*xvm*.zip" "res_mods" ".\"
	MOVE ".\res_mods" ".\XVM_BASE"
	
	DEL /Q ".\XVM_BASE\configs\xvm\configs*"
	RD /S /Q ".\XVM_BASE\configs\xvm\sirmax"
	RENAME ".\XVM_BASE\mods\shared_resources\xvm\res\SixthSense.png.sample" "SixthSense.png"
	RENAME ".\XVM_BASE\configs\xvm\xvm.xc.sample" "xvm.xc"
	COPY /Y ".\XVM_BASE\configs\xvm\default\vehicleNames.xc" "XVM_KOTO_CONFIG\configs\xvm\Kotyarko_O\vehicleNames.xc"
	
	@Echo "Info: End unpacking."
	GOTO:_Exit

:_Exit
	@Echo+
	@Pause
	GOTO:EOF
